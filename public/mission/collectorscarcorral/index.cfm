<cfsetting showdebugoutput="no">
<cfsilent>
<cfparam name="url.page" default="home">
<cfparam name="url.section" default="">
</cfsilent>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Collectors Car Corral</title>
<meta name="description" content="Collectors Car Corral is a high-end vehicle storage facility dedicated to providing a state of the art, highly secure, safe, clean and climate controlled environment for automative enthusiasts to store their vehicles." />
<link rel="stylesheet" href="/css/styles.css" />
<link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/swipebox.css">
<!--- Font: Raleway --->
<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700' rel='stylesheet' type='text/css'>
<!--- Font: Source Sans Pro --->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
<!--- Font: YRSA ---> 
<script src="https://use.typekit.net/fcm3zzz.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
</head>

<body<cfif url.page is "home"> id="home"</cfif>>
<cfinclude template="includes/header.cfm">
<cfinclude template="display/dsp_#url.page#.cfm">
<cfinclude template="includes/footer.cfm">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="/js/jquery.cycle2.min.js"></script>
<script src="/js/scripts.js"></script>
<script src="/js/jquery.swipebox.js"></script>

</body>
</html>