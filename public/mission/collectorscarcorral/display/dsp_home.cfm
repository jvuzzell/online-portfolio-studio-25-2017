<div class="home">

	<section class="slider-main cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-timeout="5000" data-cycle-slides="> .slide">
		
		<div class="slide" style="background-image: url(images/ccc-2016-facility-9.jpg);">
			<div class="content">
				<h1>Protect Your Investment.</h1>
				<p>From the moment you walk in you'll be confident your vehicle is in good hands.</p>
				<a href="/services-features" class="btn">View Services</a>
			</div>
			<div class="homepage-screen"></div>
		</div>
		<div class="slide" style="background-image: url(images/home-slide-2.png);">
			<div class="content">
				<h1>Protect Your Investment.</h1>
				<p>From the moment you walk in you'll be confident your vehicle is in good hands.</p>
				<a href="/services-features" class="btn">View Services</a>
			</div>
			<div class="homepage-screen"></div>
		</div>
		<div class="slide" style="background-image: url(images/ccc-2016-facility-5.jpg);">
			<div class="content">
				<h1>Protect Your Investment.</h1>
				<p>From the moment you walk in you'll be confident your vehicle is in good hands.</p>
				<a href="/services-features" class="btn">View Services</a>
			</div>
			<div class="homepage-screen"></div>
		</div>
		<div class="slide" style="background-image: url(images/ccc-2016-facility-12.jpg);">
			<div class="content">
				<h1>Protect Your Investment.</h1>
				<p>From the moment you walk in you'll be confident your vehicle is in good hands.</p>
				<a href="/services-features" class="btn">View Services</a>
			</div>
			<div class="homepage-screen"></div>
		</div>
		<div class="cycle-prev"></div>
    <div class="cycle-next"></div>
		<div class="cycle-pager"></div>
	</section>
	
	<section class="facility">
		<div class="wrap">
			<div class="content">
				<h2><span>State of the Art</span> <br>Vehicle Storage Facility</h2>
				<div class="content-int">
					<p>CCC is a high end vehicle storage facility dedicated to providing a state of the art, highly secure, safe, clean and climate controlled environment for automotive enthusiasts to store their vehicles.</p>
					<a href="/about-us" class="btn btn-black">About Us</a>
				</div>
			</div>
		</div>
		<img src="images/home-intro.png" class="img">
	</section>
	
	<section class="storage">
		<div class="wrap">
			<h2>Luxury & Classic Vehicle Storage</h2>
		<h5>Our professional staff has decades of experience in the automotive and motorsport industries. From the moment you walk in you'll experience true five-star customer service and you will be confident your vehicle is in good hands.</h5>
			<div class="group">
				<div class="col access">
					<img src="images/home-services-icon-access.png" alt="24 Hour Access">
					<h3>24 Hour Video Access</h3>
					<p>24 hour access to Live Video Stream of Your Vehicle</p>
				</div>
				<div class="col space">
					<img src="images/home-services-icon-space.png" alt="25,000 Sq Ft">
					<h3>25,000 Sq Ft</h3>
					<p>Various Storage Sizes to Fit Your Needs</p>
				</div>
				<div class="col secure">
					<img src="images/home-services-icon-secure.png" alt="Highly Secure">
					<h3>Highly Secure</h3>
					<p>Monitored Commercial Grade Security System</p>
				</div>
			</div>
			<div class="btn-links">
				<a href="/services-features" class="btn btn-white">View All Services</a>
				<a href="/pricing" class="btn btn-white-trans">Our Pricing</a>
			</div>
		</div>
		<div class="btm-angle"></div>
	</section>
	
	<section class="showroom">
		<div class="wrap">
			<div class="content">
				<h2>Our Showroom</h2>
				<!--- <p>Our Professional staff has decades of experience in the automotive and motorsport industries. We understand how you feel about your cars. We store our cars here as well, after all, we live and breath cars!</p> --->
				<a href="/showroom" class="btn btn-black">View the Corral</a>
			</div>
		</div>
		<img src="images/home-car-right.png" class="img">
	</section>
	
	<section class="sales">
		<div class="wrap">
			<div class="content">
				<h2>Looking To Buy?</h2>
				<!--- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex.</p> --->
				<a href="/vehicles-for-sale" class="btn">Vehicles For Sale</a>
			</div>
		</div>
	</section>

</div>