<div class="pricing">
	<section class="shade angle hdr-img bg-img text-overlay" style="background-image:url(../images/ccc-2016-facility-25.jpg);">
			<div class="header-intro"> 
				<div class="wrap">
					<h1>Find the Best Fit <span>for You</span></h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
				</div>
			</div>
	</section>

	
		<section class="member-fees"> 
			<div class="wrap">
				<h1 class="section-header">Monthly Storage Membership Fees</h1>
								
				<h6>8'x16' STORAGE SPACE</h6>
				<p>$250</p>
		
				<h6>8'x18' STORAGE SPACE</h6>
				<p>$300</p>
		
				<h6>10'x18' STORAGE SPACE</h6>
				<p>$350</p>
		
				<h6>12'x20' STORAGE SPACE</h6>
				<p>$400</p>
		
				<h6>3'x6' Motorcycle STORAGE SPACE</h6>
				<p>$150</p>
			
			</div>
		</section>
		
		<section class="benefits"> 
			<div class="wrap">
				<h1>Membership Includes</h1>
				<ul>
					<li>24 Hour Access to Your Vehicle by Appointment</li>
					<li>24 Hour Access to Live Video Stream of your Vehicle</li>
					<li>Reserved Parking Spots</li>
					<li>Climate Controlled State of the Art Storage Facility</li>
					<li>Monitored Commercial Grade Security System</li>
					<li>Professionally Escorted by a CCC Rep during Check in/Check Out of Your Vehicle</li>
					<li>Access to our Conference Space by Appointment</li>
					<li>Complimentary Coffee and Snacks in our Upscale Waiting Area</li>
				</ul>
				<p><strong>5&#37;</strong> for accounts paid 6 months in advance</p>
				<p><strong>10&#37;</strong> for accounts paid 12 months in advance</p>
			</div>
		</section>
		
		<div class="btn-links">
			<a href="/showroom" class="btn">View our Features</a>
		</div>		
		
		<div class="repeat-bg">
			<section class="charges"> 
				<div class="wrap">
					<h1 class="section-header">Monthly Services Charges</h1>
					<div class="col col-left"> 
						<div class="item">
							<h6>CCC Trickle Charger Package</h6>
							<p>Includes use of premium trickle charger with electric provided to your vehicle. <span>$25</span></p>
						</div>
						<div class="item">
							<h6>Car Covers</h6>
							<p>All sizes available. <span>$99</span></p>
						</div>
						<div class="item">
							<h6>VIP package</h6>
							<p>Includes CCC trickle charger package and four vehicle washes per month. <span>$75</span></p>
						</div>
					</div>
					<div class="col col-right"> 
						<div class="item">
							<h6>Personal Trickle Charger</h6>
							<p>Members may bring in their personal trickle charger. Must be approved by a CCC rep. <span>$15</span></p>
						</div>
						<div class="item">
							<h6>Car Capsule Package</h6>
							<p>A touchless car cover with its patented self-supporting air chambers and continuous filtered air flow system that completely protects your vehicle from dings, dirt, dust, corrosion, mildew, musty odors, rust and finger prints. (Minimum purchase must be a 10' X 18' storage space) <br/><span>9.5' X 16' $50.00  9.5' X 18' $65.00  9.5' X 20' $75.00</span></p>
						</div>

					</div>
				</div>
				
			</section>
			
		</div>
		<div id="additonal_services" style="position: relative; top: 2.5%;"></div>
		<section class="additional-services"> 
			<div class="wrap">
				<h1>Additional Services</h1>
				<div class="item">
					<h4>Auto Detailing</h4>
					<img src="images/diamonddetail-logo.png"/>
					<p>Provided by <a href="http://diamonddetail.com/" target="_blank">Diamond Detail</a></p>
					<h6>Please visit their website to view all services.</h6>
				</div>
				<div class="item">
					<h4>Collision Repair/Paintless Dent Removal</h4>
					<img src="images/sdrauto-logo.png"/>
					<p>Service provided by <a href="http://sdrauto.com/" target="_blank">SDR Auto</a></p>
					<h6>Please visit their website to view all services.</h6>
				</div>
			</div>
		</section>
		
		<div class="btn-links">
			<a href="/contact-us" class="btn btn-transparent">Schedule a Tour</a>
		</div>	
	

	<section class="push-footer">&nbsp;</section>
</div>
