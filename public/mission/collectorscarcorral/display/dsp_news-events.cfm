<div class="news-events">
	<div class="repeat-bg">
		<section class="hdr-img">
			<img class="mobile-hide" src="images/hdr-news.png">
			<img class="mobile" src="images/hdr-news-mobile-640px.png">
		</section>
		
		<section class="header-intro"> 
			<div class="wrap">
				<h1>News & <span>Events</span></h1>
				<p>From classics to exotics, motorcycles to hummers, we have the storage space you are looking for.</p>
			</div>
		</section>
		
		<section class="news">
			<div class="wrap">	
				<div class="border-shadow articles">
					<div class="article">
						<div class="feature-image"><img src="http://placehold.it/400x300"/></div>
						<div class="content">
							<h3>Classic Car Weekend</h3>
							<h5>May 16 - 22, 2016</h5>
							<p>
							This popular car show features over 3,400 hot rods, customs, classics, street machines, muscle cars and more. Live entertainment, celebrity guests, special attractions, boardwalk parades, manufacturers vendor midway and more.  Admission.  Boardwalk Parades on Thursday, Friday, and Saturday mornings from 7am to 10am.  So many things to see and do, so make sure you cruise on down to Ocean City, MD, for the 25th Annual Cruisin' Ocean City. 
							</p>
							<h6>For additional information, visit www.cruisinoceancity.com, or call 410-289-2800 or 800-626-2326</h6>
							<a class="cta" href="">Visit Website</a>
						</div>
					</div>
					
					<div class="article">
						<div class="feature-image"><img src="http://placehold.it/400x300"/></div>
						<div class="content">
							<h3>Opening this Weekend!</h3>
							<h5>In the News</h5>
							<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
							</p>

							<a class="cta" href="">Visit Website</a>
						</div>
					</div>
					
					<div class="article">
						<div class="feature-image"><img src="http://placehold.it/400x300"/></div>
						<div class="content">
							<h3>Classic Car Weekend</h3>
							<h5>May 16 - 22, 2016</h5>
							<p>
							This popular car show features over 3,400 hot rods, customs, classics, street machines, muscle cars and more. Live entertainment, celebrity guests, special attractions, boardwalk parades, manufacturers vendor midway and more.  Admission.  Boardwalk Parades on Thursday, Friday, and Saturday mornings from 7am to 10am.  So many things to see and do, so make sure you cruise on down to Ocean City, MD, for the 25th Annual Cruisin' Ocean City. 
							</p>
							<h6>For additional information, visit www.cruisinoceancity.com, or call 410-289-2800 or 800-626-2326</h6>
							<a class="cta" href="">Visit Website</a>
						</div>
					</div>
					
					<div class="article">
						<div class="feature-image"><img src="http://placehold.it/400x300"/></div>
						<div class="content">
							<h3>Classic Car Weekend</h3>
							<h5>May 16 - 22, 2016</h5>
							<p>
							This popular car show features over 3,400 hot rods, customs, classics, street machines, muscle cars and more. Live entertainment, celebrity guests, special attractions, boardwalk parades, manufacturers vendor midway and more.  Admission.  Boardwalk Parades on Thursday, Friday, and Saturday mornings from 7am to 10am.  So many things to see and do, so make sure you cruise on down to Ocean City, MD, for the 25th Annual Cruisin' Ocean City. 
							</p>
							<h6>For additional information, visit www.cruisinoceancity.com, or call 410-289-2800 or 800-626-2326</h6>
							<a class="cta" href="">Visit Website</a>
						</div>
					</div>
				</div>
			</div> 
		</section>

		<div class="btn-links">
			<a href="/showroom" class="btn">View our Showroom</a>
			<a href="/about-us" class="btn btn-transparent">Learn More About Us</a>
		</div>		
	
		<section class="push-footer">&nbsp;</section>
	</div>
</div>