<div class="news-events for-sale">
	<div class="repeat-bg">
		<section class="hdr-img">
			<img class="mobile-hide" src="images/hdr-vehicles.png">
			<img class="mobile" src="images/hdr-vehicles-mobile-640px.png">
		</section>
		
		<section class="header-intro"> 
			<div class="wrap">
				<h1>Vehicles <span>for Sale</span></h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
			</div>
		</section>
		
		<section class="news">
			<div class="wrap">	
				<div class="border-shadow articles">
					<div class="article">
						<div class="feature-image"><img src="http://placehold.it/400x300"/></div>
						<div class="content">
							<h3>1965 Shelby Cobra For Sale</h3>
							<p>
							This car is a Cobra Replica from Factory Five Racing in silver with black. Built from new using only the best.
							</p>
							<h6>Call Joe at 443-443-4433 or email at <a  href="">joe@joe.com</a></h6>
						</div>
					</div>
					
					<div class="article">
						<div class="feature-image"><img src="http://placehold.it/400x300"/></div>
						<div class="content">
							<h3>1965 Shelby Cobra For Sale</h3>
							<p>
							This car is a Cobra Replica from Factory Five Racing in silver with black. Built from new using only the best.
							</p>
														<h6>Call Joe at 443-443-4433 or email at <a  href="">joe@joe.com</a></h6>
						</div>
					</div>
					
					<div class="article">
						<div class="feature-image"><img src="http://placehold.it/400x300"/></div>
						<div class="content">
							<h3>1965 Shelby Cobra For Sale</h3>
							<p>
							This car is a Cobra Replica from Factory Five Racing in silver with black. Built from new using only the best.
							</p>
							<h6>Call Joe at 443-443-4433 or email at <a  href="">joe@joe.com</a></h6>
						</div>
					</div>
					
					<div class="article">
						<div class="feature-image"><img src="http://placehold.it/400x300"/></div>
						<div class="content">
							<h3>1965 Shelby Cobra For Sale</h3>
							<p>
							This car is a Cobra Replica from Factory Five Racing in silver with black. Built from new using only the best.
							</p>
							<h6>Call Joe at 443-443-4433 or email at <a  href="">joe@joe.com</a></h6>
						</div>
					</div>
					
					<div class="article">
						<div class="feature-image"><img src="http://placehold.it/400x300"/></div>
						<div class="content">
							<h3>1965 Shelby Cobra For Sale</h3>
							<p>
							This car is a Cobra Replica from Factory Five Racing in silver with black. Built from new using only the best.
							</p>
							<h6>Call Joe at 443-443-4433 or email at <a  href="">joe@joe.com</a></h6>
						</div>
					</div>
					
				</div>
			</div> 
		</section>
		
		<section class="push-footer">&nbsp;</section>
	</div>
</div>