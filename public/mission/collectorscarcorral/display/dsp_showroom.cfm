<div class="showroom">
	<section class="angle hdr-img bg-img" style="background-image:url(../images/ccc-2016-facility-23.jpg);">
		
	</section>
	
	<section class="page-content">
		<div class="wrap">	
			<div class="intro">
				<h1>Our <span>Showroom</span></h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris egestas faucibus magna et luctus. Donec non risus congue, vehicula magna id, blandit arcu. Nam ligula sem, placerat a ultrices pulvinar, ornare et justo. Sed vel purus diam. Fusce ornare bibendum dignissim. </p>
			</div>
		</div>
	</section>
	
	<div class="repeat-bg">
		<section class="galleries">
			<div class="wrap">
				<div class="grid">
					<div class="item" data-galleryid="1">
						<div class="album-cover">
							<img src="http://placehold.it/375x276">
						</div>
						<div class="album-title"> 
							<p>Cars Gallery</p>
							<div class="icon"></div>
						</div>
					</div>
					<div class="item" data-galleryid="2">
						<div class="album-cover">
							<img src="http://placehold.it/375x276">
						</div>
						<div class="album-title"> 
							<p>Facility Gallery</p>
							<div class="icon"></div>
						</div>
					</div>
					<div class="item" data-galleryid="3">
						<div class="album-cover">
							<img src="http://placehold.it/375x276">
						</div>
						<div class="album-title"> 
							<p>Event Gallery</p>
							<div class="icon"></div>
							
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	
	<div class="repeat-bg">
		<section class="features">
			<div class="wrap">
				<div class="intro">
					<h1>Our <span>Facility Features</span></h1>
				</div>
				<div class="features-grid">
					<div class="item">
						<div class="icon" style="background-image: url(images/icon-climate.png);"></div>
						<div class="desc">
							<h2>Climate Controlled Facility</h2>
							<p>State of the Art Climate Controlled Facility with an Insulated Roof and Ventilation System</p>
						</div>
					</div>
					<div class="item">
						<div class="icon" style="background-image: url(images/icon-secure.png);"></div>
						<div class="desc">
							<h2>Highly Secure Building</h2>
							<p>Monitored Commercial Grade Security System</p>
						</div>
					</div>
					<div class="item">
						<div class="icon" style="background-image: url(images/icon-storage.png);"></div>
						<div class="desc">
							<h2>25,000 Sq Ft Storage Facility</h2>
							<p>Various Storage Sizes to Fit Your Needs</p>
						</div>
					</div>
					<div class="item">
						<div class="icon" style="background-image: url(images/icon-livestream.png);"></div>
						<div class="desc">
							<h2>Live Video Stream</h2>
							<p>24 Hour Access to Live Video Stream of Your Vehicle</p>
						</div>
					</div>
					<div class="item">
						<div class="icon" style="background-image: url(images/icon-fire.png);"></div>
						<div class="desc">
							<h2>Fire Protection System</h2>
							<p>Centrally Monitored Fire Protection System</p>
						</div>
					</div>
					<div class="item">
						<div class="icon" style="background-image: url(images/icon-concierge.png);"></div>
						<div class="desc">
							<h2>Professional Concierge Services</h2>
							<p>Courteous and Profesional Staff Members</p>
						</div>
					</div>
				</div>
				
				<div class="btn-links">
					<a href="/services-features" class="btn">View All Features</a>
				</div>
				
			</div>
		</section>

		<section class="services showroom-bg">
			<div class="wrap">
				<div class="col col-centered quote">
					<p>Consectetur adipisicing elit<br/>
					Sed do eiusmod temporincid idunt ut labore.</p>
					<a href="/news-events" class="btn btn-white">View Upcoming Events</a>
				</div>
			</div>
			
		</section>
	</div><!-- end repeat-bg -->
</div>

<a href="http://placehold.it/600x600/ff0000/ffffff?text=image1" rel="gallery-1" title="" class="swipebox"></a>
<a href="http://placehold.it/1000x600/ff0000/ffffff?text=image2" rel="gallery-1" title="" class="swipebox"></a>
<a href="http://placehold.it/600x800/ff0000/ffffff?text=image3" rel="gallery-1" title="" class="swipebox"></a>
<a href="http://placehold.it/600x900/00ff00/ffffff?text=image4" rel="gallery-2" title="" class="swipebox"></a>
<a href="http://placehold.it/600x1000/00ff00/ffffff?text=image5" rel="gallery-2" title="" class="swipebox"></a>
<a href="http://placehold.it/600x600/00ff00/ffffff?text=image6" rel="gallery-2" title="" class="swipebox"></a>
<a href="http://placehold.it/900x600/0000ff/ffffff?text=image7" rel="gallery-3" title="" class="swipebox"></a>
<a href="http://placehold.it/700x600/0000ff/ffffff?text=image8" rel="gallery-3" title="" class="swipebox"></a>
<a href="http://placehold.it/600x600/0000ff/ffffff?text=image9" rel="gallery-3" title="" class="swipebox"></a>