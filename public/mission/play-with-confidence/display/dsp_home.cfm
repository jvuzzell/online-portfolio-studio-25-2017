<cfset twitterDescription="https://twitter.com/home?status=Defend%20the%20Game%20with%20STX.%20Coming%20November%202016.%20Click%20to%20find%20out%20more.%20">
<cfset subject="Defend the Game with STX. Coming November 2016.">
<cfset body="Find%20out%20more%20about%20what%27s%20coming%20for%20defenders%20this%20November%2E%20Win%20an%20unreleased%20complete%20stick%20%26%20exclusive%20STX%20prizes%2E">

<cfparam name="msg" default="">
<cfparam name="success" default="0">
<cfif isDefined('form.email')>
	<cfif form.contact_name is not "">
		<cfset msg = "Invalid Submission">
	<cfelse>
		<cfsavecontent variable="localscope.soapRequest">
		<cfoutput>
      <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Body>
          <Subscriber_Add_Subscriber xmlns="http://BlueSkyFactory.Publicaster7.LegacyAPI/Subscriber/">
            <AccountID>L05p4NgaQEw=</AccountID>
            <PrimaryKey>Email</PrimaryKey>
            <Action>AppendAndUpdate</Action>
            <ListIDs>
              <int>1</int>
            </ListIDs>
            <IncludeDeleted>0</IncludeDeleted>
            <FieldNames>
              <string>Email</string>
              <string>FirstName</string>
              <string>LastName</string>  
              <cfif isDate(form.birthday)><string>Birthday</string></cfif>
              <string>State</string>  
              <string>Zipcode</string>        
              <string>Source</string>
              <string>womens_lacrosse</string>
              <string>fortress_600_stick</string>
            </FieldNames>
            <FieldValues>
              <string>#form.email#</string>
              <string>#form.fname#</string>
              <string>#form.lname#</string>  
              <cfif isDate(form.birthday)><string>#form.birthday#</string></cfif>
              <string>#form.state#</string>  
              <string>#form.zip#</string>        
              <string>fortress_600_stick</string>
              <string>1</string>
              <string>1</string>
            </FieldValues>
          </Subscriber_Add_Subscriber>
        </soap:Body>
      </soap:Envelope>
    </cfoutput>
    </cfsavecontent>
        
    <cfhttp url="http://api7.publicaster.com/Subscriber/Subscriber.asmx?wsdl" method="POST" resolveurl="NO" throwonerror="yes" useragent="Axis/1.1">
    <cfhttpparam type="header" name="SOAPAction" value="http://BlueSkyFactory.Publicaster7.LegacyAPI/Subscriber/Subscriber_Add_Subscriber"> 
    <cfhttpparam type="xml" name="body" value="#localscope.soapRequest#">
    </cfhttp>
    <cfset localscope.soapresponse = XMLParse(cfhttp.FileContent) />

		<cfset msg = "Thank you for signing up!<br />">
		<cfset success = 1>
	</cfif>
</cfif>
<script>
function validateSignupForm(frm) {
	var emailRX = /^([\w]+)(.[\w]+)*@([\w]+)(.[\w]{2,3}){1,2}$/;
	var dateRX = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;
	var zipRX = /^\d{5}$/ ;
	var msg = "";
		if (frm.email.value == "" || emailRX.test(frm.email.value) != true) {
			msg += "Please enter a valid email<br/>";
		}
		if (frm.fname.value == "" || frm.fname.value == "FIRST NAME") {
			msg += "Please enter a first name<br/>";
		}
		if (frm.lname.value == "" || frm.lname.value == "LAST NAME") {
			msg += "Please enter a last name<br/>";
		}
		if (frm.birthday.value == "" || dateRX.test(frm.birthday.value) != true) {
			msg += "Please enter a valid birthday<br/>";
		}
		if (frm.state.value == "" || frm.state.value == "STATE") {
			msg += "Please enter a state<br/>";
		}
		if (frm.zip.value == "" || zipRX.test(frm.zip.value) != true) {
			msg += "Please enter a valid 5-digit zip code<br/>";
		}
		if(msg == "") {	return true;} 
		else {
			msg += "<br/>";
			document.getElementById('ferror').innerHTML = msg; 
			return false;}
	}
</script>


<div class="scrolling">
	<section class="frame frame-1">
		<img class="mobile-hide lax-head" src="images/translucent-lax-head2.png" />
		<img class="mobile lax-head" src="images/translucent-lax-head-mobile_.png"/>
		<div id="text-box-1">
			<img id="text-lockup" src="images/play-with-confidence.png" alt="Play with Confidence"/>
			<h1><span style="font-weight:600;">Defend</span> The Game</h1>
			<h2>November 2016</h2>
			<p><em>Find out how to win</em></p>
			<div id="cta-animation"> 
				<img class="mobile-hide" src="images/scroll-indicator.gif" alt="Continue to Scroll"/>
			</div>
		</div>
		<div id="athlete-id">
			<p>Sloane Serpe</p>
			<p>Team USA/Long Island Sound</p>
		</div>
		
		<div class="bg-img" id="smoke-1"></div>
		<img class="sloane mobile-hide" src="images/sloane.png" alt="Sloane Serpe" />
		<img class="sloane mobile" src="images/sloane-mobile.png" alt="Sloane Serpe" />
		<div class="bg-img" id="smoke-2"></div>
	</section>

	<section class="frame frame-2 bg-img">

		<div id="text-box-2">
			<h3 style="position:relative; z-index:2;">Sign up for your chance to win STX prizes that will help you <span class="mobile"><br/></span><strong>defend the game.</strong></h3>
			<img id="week2-image" src="images/sloane-grand-prize.png"/>
			<!---
			<div class="callout">
				<img class="border-img" src="images/callout-border.png"/>
				<h4>WIN AN UNRELEASED <span class="mobile"><br/></span><strong>COMPLETE STICK</strong></h4>
				<img class="border-img" src="images/callout-border.png"/>
			</div>
	
			<p id="social-callout">Follow us on Instagram <strong>@stxwlax</strong> for your chance to win an unreleased complete stick that will help you <strong class="mobile-emphasis">Defend the Game.</strong></p><p><strong>Winner announced on Instagram October 11th.</strong></p>
		
			<a href="https://www.instagram.com/stxwlax/" class="btn" onClick="ga('send', 'event', { eventCategory: 'fortress-600-follow-us-instagram', eventAction: 'click', eventLabel: 'fortress-600-follow-us'});"><img src="images/instagram-icon.png"/><span>Follow Us</span></a>
			
			<p class="disclaimer"><em>Check back on October 11th when we will reveal the Grand Prize.</em></p>

--->
		</div>

		<a name="signup"></a>
		<div id="form-wrap">

			<h4>WIN THE <strong>GRAND PRIZE</strong></h4>
			<p>Sign up to win one-on-one defensive training with <br class="mobile-hide"/> 3x All-American Sloane Serpe, an autographed Long Island Sound jersey, <strong style="color:#ffffff;">and an unreleased complete stick.</strong></p>
			<p class="red">Winner announced on Instagram October 18th.</p>
      <div id="ferror" class="red"><cfif isDefined('msg')><cfoutput>#msg#</cfoutput></cfif></div>
      <form method="post" id="form-1" action="#signup" onsubmit="return validateSignupForm(this);">
				<div class="grid"> 
					<div class="col-2">
						<input type="text" name="email" placeholder="Email *"/>
						<input type="text" name="fname" placeholder="First Name *" />
						<input type="text" name="lname" placeholder="Last Name *" />
						
					</div>
					<div class="col-2">
						<input type="text" name="birthday" placeholder="Birthday (MM/DD/YYYY) *" />
						<input type="text" name="state" placeholder="State *" />
						<input type="text" name="zip" placeholder="Zip Code *" />
					</div>
				</div>
      	<div style="display: none"><input type="text" name="contact_name" value=""></div>
				<input type="submit" name="submit" value="SIGN UP" class="btn"/>
			</form>
			<a class="btn-red" href="images/SweepRules_DefendTheGame2016.pdf" target="_blank">Details & Rules</a>
		</div>
		
		<img id="smoke-3" src="images/smoke-screen-3_.png"/>
		<div class="bg-img mobile" id="smoke-4"></div>
		
	</section>

		<section class="social">
		<div class="container">
			<div class="links">
				<cfoutput>
					<a href="https://www.facebook.com/sharer.php?u=http://dev.stx.com/play-with-confidence/" target="_blank"><img src="images/fb.png"></a>
					<a href="#twitterDescription#" target="_blank"><img src="images/tw.png"></a>
					<a href="mailto:?subject=#subject#&body=#body#"><img src="images/mail.png"></a>
				</cfoutput>
			</div>
		</div>
        
	</section> <!-- /social -->
</div> <!-- end scrolling -->
<div class="bg-img mobile-hide" id="smoke-4"></div>