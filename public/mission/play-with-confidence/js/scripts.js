var contentContainer,overlayContainer;

$(function(){
	
	$('a.iframe').on('click',function(event){
		var winWidth = $(window).width();
		if(winWidth <= 640){
			event.preventDefault();
			window.location.href = $(this).attr('data-link');
			$.prettyPhoto.close();
			return false;
		}
	});
	
	currentslide = 'mens';
	
	contentContainer = $('.main-content .content');
	overlayContainer = $('.main-content aside');
	$('.main-content .content .row h1 span').on('mouseover',function(){
		if(!contentContainer.hasClass('busy')){
			var selection = $(this).parent();
			var row = selection.closest('.row');
			var value = row.attr('data-layer');
			updateImage(row,value);
		}
	});
	$(document).on('click',function(event){
		if(!contentContainer.hasClass('busy')){
			resetImage();
		}
	});
});

function updateImage(row,value){
	contentContainer.addClass('busy');
	if(!row.hasClass('active')){
	
		if(value === 'flexibility'){
			row.find('img.hide').stop().slideDown();
			row.siblings().stop().slideUp();
		}
	
		row.addClass('active').siblings().removeClass('active');
		row.find('h1').addClass('active');
		row.siblings().find('h1').removeClass('active');
		var activeOverlay = overlayContainer.find('img[data-layer="' + value + '"]');
		activeOverlay.stop().animate({'opacity':1},function(){
			contentContainer.removeClass('busy');
		});
		activeOverlay.siblings().not('.default').stop().animate({'opacity':0});
	}
	else{
		
		if(value === 'flexibility'){
			contentContainer.find('img.hide').stop().slideUp();
			row.siblings().stop().slideDown();
		}
		
		row.removeClass('active').find('h1').removeClass('active');
		overlayContainer.children('.overlay').not('.default').stop().animate({'opacity':0},function(){
			contentContainer.removeClass('busy');
		});
	}
}

function resetImage(){
	contentContainer.addClass('busy');
	contentContainer.find('img.hide').stop().slideUp();
	contentContainer.find('.row,h1').removeClass('active');
	contentContainer.find('.row').stop().slideDown();
	overlayContainer.children().not('.default').stop().animate({'opacity':0},function(){
		contentContainer.removeClass('busy');
	});
}