<?php

require ("app/library/utilities/get-uri.php");

	$baseUrl = getCurrentUri();
	$routes = array();
	$routes = explode('/', $baseUrl);

if(!empty($routes[1])){
	switch($routes[1]){
		case 'about':
		$action = 'about';
		break;
		case 'resume':
		$action = 'resume';
		break;
		case 'contact':
		$action = 'contact';
		break;
		case 'project':
		$action = 'single-project';
		$id = $routes[4];
		break;
		case 'test':
		$action ='test';
		break;
		case 'case-studies':
		$action = 'case-studies';
		break;

		case 'print':
		$action = 'display-projects';
		$display_project_type = 'print';
		$scrolltoprojects = true;
		break;
		case 'web':
		$action = 'display-projects';
		$display_project_type = 'web';
		$scrolltoprojects = true;
		break;
		case 'identity':
		$action = 'display-projects';
		$display_project_type = 'identity';
		$scrolltoprojects = true;
		break;
		case 'branding':
		$action = 'display-projects';
		$display_project_type = 'identity';
		$scrolltoprojects = true;
		break;

		//project short code
		case 'entertainment-cruises':
		$action = 'single-project';
		$id = 0;
		break;
		case 'pratt-street-ale-house':
		$action = 'single-project';
		$id = 1;
		break;
		case 'columbia-ale-house':
		$action = 'single-project';
		$id = 2;
		break;
		case 'baltimore-city-part-1':
		$action = 'single-project';
		$id = 3;
		break;
		case 'baltimore-city-part-2':
		$action = 'single-project';
		$id = 4;
		break;
		case 'oliver-breweries':
		$action = 'single-project';
		$id = 5;
		break;
		case 'lexi-and-her-donkey':
		$action = 'single-project';
		$id = 6;
		break;
		case 'putnam-pantry-packaging':
		$action = 'single-project';
		$id = 7;
		break;
		case 'logo-collection-1':
		$action = 'single-project';
		$id = 8;
		break;
		case 'logo-collection-2':
		$action = 'single-project';
		$id = 21;
		break;
		case 'body-mind-spirit-therapy':
		$action = 'single-project';
		$id = 10;
		break;
		case 'be-more-sugar':
		$action = 'single-project';
		$id = 11;
		break;
		case 'aiga-converse':
		$action = 'single-project';
		$id = 12;
		break;
		case 'aiga-blend':
		$action = 'single-project';
		$id = 13;
		break;

		case 'ishimoto-massage-therapy':
		$action = 'single-project';
		$id = 15;
		break;
		case 'putnam-pantry-website':
		$action = 'single-project';
		$id = 16;
		break;
		case 'uzzell-portfolio':
		$action = 'single-project';
		$id = 17;
		break;
		case 'myhtml-email-app':
		$action = 'single-project';
		$id = 18;
		break;
		case 'cakewalk-app':
		$action = 'single-project';
		$id = 19;
		break;
		case 'barnett-campaign':
		$action = 'single-project';
		$id = 20;
		break;
		case 'becoming-68':
		$action = 'single-project';
		$id = 22;
		break;
		case 'surgeon-700':
		$action = 'single-project';
		$id = 23;
		break;
		case 'dreamworks-create':
		$action = 'single-project';
		$id = 24;
		break;
		case 'mission-media-emails':
		$action = 'single-project';
		$id = 25;
		break;
		case 'collectors-car-corral':
		$action = 'single-project';
		$id = 26;
		break;
		case 'everyman-theatre':
		$action = 'single-project';
		$id = 27;
		break;


		//default
		default:
		$action = 'frontpage';
		break;
	}
} else {
	$action = 'frontpage';
}

require ($_SERVER['DOCUMENT_ROOT']."/app/library/utilities/config.php");

require_once(LIBRARY_PATH.'project-profiles.php');
if($action == "single-project"){
	if(empty($project_profiles[$id]) || $project_profiles[$id]['published'] !== "published"){
		if($project_profiles[$id]['published'] === "draft" && $_SERVER["REMOTE_ADDR"] == "73.212.172.179"){
			//continue
		} else {
			header('Location: http://joshuauzzell.com');
		}
	}
}


require (DIRECTORY_PATH.'header.php');
if($action == 'frontpage'){
	include (TEMPLATES_PATH."$action.php");
}else{
	require (TEMPLATES_PATH.'parts/navigation/main-nav.php');
	require (TEMPLATES_PATH."$action.php");
}
require (DIRECTORY_PATH.'footer.php');
?>
