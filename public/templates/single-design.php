<?php
	echo " 
			<div id='top' class='col-xs-12 col-sm-10 single-project single-design'>
				<section class='row'>
					<div class='project-category col-xs-12 col-md-4'>
						<header class='project-name'>
						 	<h4 class='title'>".$singleProject['title']."</h4>
						 	<h5 class='subtitle'>".$singleProject['client']."</h5>
					 	</header>"; 
		echo"			</div>
						<article class='single-summary col-xs-12 col-md-8'>
							<div class='textbox'>
								<h5>Details</h5>
								<p class='challenge'><span>".$singleProject['process']."</span></p>
							</div>
							<p class='role'>";
								echo"<b>Categories: </b><span class='text-muted'>";
								$categories="";
								
								foreach ($singleProject['categories'] as $category){ 
									$categories .= $category.", ";
								}
								$categories = substr($categories, 0, -2);
								echo $categories;
								
								echo"
									</span>
									<br><b>Roles: </b><span class='text-muted'>";
								$roles="";
								
								foreach ($singleProject['roles'] as $role){ 
									$roles .= $role.", ";
								}
								$roles = substr($roles, 0, -2);
								echo $roles;
								
			echo"					</span>
							</p>";
			echo"		</article>
				</section>
			
				<hr>
				";
			echo"
				<section class='row project-slider'>	
					<div class='project-slides "; 
						if($singleProject['space-images'] == true){echo "space-images";}
					echo" col-xs-12'> 	
				";
			
					foreach ($singleProject['images'] as $image){ 
						echo '<img src="'.$image.'"/>';
					}
			echo
				"
					</div>
				</section>	
			</div>";		
?>
