<aside class="mysidebar hidden hidden-xs hidden-sm">
	<div id="sidebar-wrap"> 
		<a class="custom-button category-select" id="display-all-featured">
			Featured
		</a>
		<a class="custom-button category-select" id="only-interactives">
			Interactives
		</a>
		<a class="custom-button category-select" id="only-branding">
			Branding
		</a>
		<a class="custom-button category-select" id="only-print">
			Print
		</a>
	</div>
	
</aside>

