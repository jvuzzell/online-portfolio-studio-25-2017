<div class="jumbotron">
	<nav class="row nav-main">
		<div class="col-xs-2 col-sm-4 col-md-4 nav-logo">
			<a href="<?php echo $home_url; ?>"><img src="<?php echo $paths['images']['layout'].'joshua-uzzell-logo-white.png';?>" alt="Joshua Uzzell Graphic Designer & Web Developer" /></a>
		</div>
		<div class="col-xs-10 col-sm-8 col-md-8 page-links">
			<ul class="list-unstyled list-inline">
				<!-- <li><a href="<?php print($home_url.'/about');?>">About</a></li>
				<li><a href="<?php print($home_url.'/resume.pdf');?>" target="_blank">Resume</a></li>
				<li><a href="https://github.com/jvuzzell" target="_blank">GitHub</a></li> -->
			</ul>
		</div>
	</nav>

	<div class="row">
		<div id="intro-text" class="col-xs-4">
			&nbsp;
		</div>
		<div style="width:100%;">
			<div id="buildings-left"><img src="<?php print($paths["images"]["layout"].'/hero/hero_img_nov_2015_buildings-left-alt.png');?>" /></div>
			<div id="clouds-left"><img src="<?php print($paths["images"]["layout"].'/hero/hero_img_nov_2015_clouds-left.png');?>" /></div>
			<div id="hero-word-mark"><img src="<?php print($paths["images"]["layout"].'/hero/hero_img_nov_2015_word-mark.png');?>" /></div>
			<div id="hero-selfie" ><img src="<?php print($paths["images"]["layout"].'/hero/hero_img_nov_2015_selfie-w-outer-glow.png');?>" /></div>
			<div id="buildings-right" ><img src="<?php print($paths["images"]["layout"].'/hero/hero_img_nov_2015_buildings-right.png');?>" /></div>
			<div id="clouds-right"><img src="<?php print($paths["images"]["layout"].'/hero/hero_img_nov_2015_clouds-right.png');?>" /></div>
			<div id="roads" class=""><img src="<?php print($paths["images"]["layout"].'/hero/hero_img_nov_2015_roads.png');?>" /></div>
		</div>
	</div>
</div>
