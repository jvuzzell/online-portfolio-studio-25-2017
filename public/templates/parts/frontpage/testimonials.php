<section id="testimonials" class="row hidden-xs">
	<article class="col-xs-11">
		<img src="<?php echo $paths['images']['layout'].'speech-bubble-wht.png'?>" alt="Speech bubbles" class="header"> 
<!-- 		<h3>Endorsements</h3> -->
			<img src="<?php echo $paths['images']['layout'].'quote-mark-left.png'?>" id="quote-left">
			<img src="<?php echo $paths['images']['layout'].'quote-mark-right.png'?>" id="quote-right">
			<div class="row">
				<div id="endorsement-tabs" class="group">

				  	<div class=" col-xs-12 tab active" data-id="1">
						<div class="endorsement">
							<p>[Josh] has been a great asset to the development team, and his passion for learning new technology and pushing the envelope will be missed. He has been a key contributor to the success of some really cool projects, particularly our work with <br class="hidden-lg"> Dreamworks and STX.</p>
							
							<h6>Amy Toelson <br/><span>Digital Director, Mission Media</span></h6>
						</div>
					</div>
					
				  	<div class=" col-xs-12 tab" data-id="4">
						<div class="endorsement">
							<p>Josh continues to expand his scope of knowledge. <br>He offers his clients many creative options and approaches <br>to accomplish their goals via design.</p>
							
							<h6>Christopher Lundy <br/><span>Assistant City Solicitor, City of Baltimore</span></h6>
						</div>
					</div>
					
					<div class="col-xs-12 tab" data-id="2">
						<div class="endorsement">
							<p>Josh offers a good understanding and strong foundation <br>of core front-end development skills. He would be a good candidate <br class="hidden-sm">for an independent role or within an organization as a team player.</p>
												
							<h6>Doug Green<br/><span>Technical Reviewer for AQUENT</span></h6>
						</div>
					</div>
					
					<div class=" col-xs-12 tab" data-id="3">
						<div class="endorsement">
							<p>Josh has provided great value to our events and his ability <br>to analyze our data will prove extremely helpful as we<br> continue create new programming.</p>
						
							<h6>Stacey Fatica<br/><span>Former-President of AIGA Baltimore</span></h6>
						</div>
					</div>
					<ul class="group">
					    <li class="active"><a class="switch" data-id="1">1</a></li>
					    <li><a class="switch" data-id="2">2</a></li>
					    <li><a class="switch" data-id="3">3</a></li>
					    <li><a class="switch" data-id="4">4</a></li>
				  	</ul>

			</div>
		</div>
	</article>
</section>