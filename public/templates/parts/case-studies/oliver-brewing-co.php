<div class="case-study row" id="oliverbrew">
		<div class="row horizontal-line headline"><p class="header">Interactive Demo</p></div>
	<div class="bg-img ollie-right-hand" style="background-image:url(<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/svg/ollie_hand.svg');?>);"></div>
	<div class="activity-wrap active">
		<div class="left-nav">
			<div class="item"><p class="title">Oliver Brewing Co.<br>Tap Handles<br><span style="font-size: 18px;font-weight: 400;"><em>Custom Multiview Slider</em></span></p></div>
			<div class="item return-to-start">
				<div class="icon">
					<img src="<?php print($paths["images"]["layout"].'case-studies/icon_return-to-start.svg');?>" />
				</div>
				<p>Return to Start</p>
			</div>
			
			<div class="item grid-view">
				<div class="icon">
					<svg class="part-1" viewBox="0 0 14.95 55"><rect width="14.95" height="14.95" style="fill:#0d4a88"/><rect y="20.03" width="14.95" height="14.95" style="fill:#0d4a88"/><rect y="40.06" width="14.95" height="14.95" style="fill:#0d4a88"/></svg>
					<svg class="part-2" viewBox="0 0 14.95 55"><rect width="14.95" height="14.95" style="fill:#0d4a88"/><rect y="20.03" width="14.95" height="14.95" style="fill:#0d4a88"/><rect y="40.06" width="14.95" height="14.95" style="fill:#0d4a88"/></svg>
					<svg class="part-3" viewBox="0 0 14.95 55"><rect width="14.95" height="14.95" style="fill:#0d4a88"/><rect y="20.03" width="14.95" height="14.95" style="fill:#0d4a88"/><rect y="40.06" width="14.95" height="14.95" style="fill:#0d4a88"/></svg>
				</div>
				<p>Grid View</p>
			</div>
		</div>
		
		<!--- OLLIE --->			
		<div class="ollie active"> 
			<div class="bg-img body" style="background-image:url(<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/svg/ollie_body.svg');?>);"></div>
			<div class="head-wrap">
				<div class="bg-img head" style="background-image:url(<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/svg/ollie_head-1.svg');?>);"></div>
				<div class="bg-img head eyes" style="background-image:url(<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/svg/ollie_head-eyes-open.svg');?>);"></div>
			</div>
			<div class="left-hand">
				<div class="bg-img mug" style="background-image:url(<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/svg/ollie_mug.svg');?>);"></div>
				
			</div>
			
		</div>
			
		<!--- TAPS --->
		<div class="taps" data-view="start">
			<div class="tap position-3" data-tap="1" style="background-image:url(<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/png/tap_1.png');?>);">
				
				<div class="screen"></div>
			</div>
			<div class="tap position-3" data-tap="2" style="background-image:url(<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/png/tap_2.png');?>);">
				<div class="screen"></div>
			</div>
			<div class="tap position-3" data-tap="3"  style="background-image:url(<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/png/tap_3.png');?>);">
				<div class="screen"></div>
			</div>
			<div class="tap position-1" data-tap="4"  style="background-image:url(<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/png/tap_4.png');?>);">
				<div class="screen"></div>
			</div>
			<div class="tap position-1" data-tap="5"  style="background-image:url(<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/png/tap_5.png');?>);">
				<div class="screen"></div>
			</div>
			<div class="tap position-1" data-tap="6"  style="background-image:url(<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/png/tap_6.png');?>);">
				<div class="screen"></div>
			</div>
		</div>
		
		<!--- TAP LABELS --->
		<div class="tap-labels">
			<div class="tap-label" data-label="1">
				<img src="<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/png/label_1.png');?>" />
				<div class="screen"></div>
			</div>
			<div class="tap-label" data-label="2">
				<img src="<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/png/label_2.png');?>" />
				<div class="screen"></div>
			</div>
			<div class="tap-label" data-label="3">
				<img src="<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/png/label_3.png');?>" />
				<div class="screen"></div>
			</div>
			<div class="tap-label" data-label="4">
				<img src="<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/png/label_4.png');?>" />
				<div class="screen"></div>
			</div>
			<div class="tap-label" data-label="5">
				<img src="<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/png/label_5.png');?>" />
				<div class="screen"></div>
			</div>
			<div class="tap-label" data-label="6">
				<img src="<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/png/label_6.png');?>" />
				<div class="screen"></div>
			</div>
		</div>
		
		<!--- TAP NAMES --->
		<div class="tap-names hidden" data-tap="1">
			<div class="tap-name" data-tap="1">
				<div class="tapNameDot"></div>
				<h1>If You Want Blood</h1>
			</div>
			<div class="tap-name" data-tap="2">
				<div class="tapNameDot"></div>
				<h1>Merry Ole Ale</h1>
			</div>
			<div class="tap-name" data-tap="3">
				<div class="tapNameDot"></div>
				<h1>Ironman Pale Ale</h1>
			</div>
			<div class="tap-name" data-tap="4">
				<div class="tapNameDot"></div>
				<h1>War of 1812 - UK</h1>
			</div>
			<div class="tap-name" data-tap="5">
				<div class="tapNameDot"></div>
				<h1>War of 1812 - USA</h1>
			</div>
			<div class="tap-name" data-tap="6">
				<div class="tapNameDot"></div>
				<h1>The Spruce is Loose</h1>
			</div>
		</div>
		
		<!--- DETAIL VIEW CONTROLS --->
		<div class="cs-slider" data-slide="">
			<div class="cs-arrow previous">
				<svg viewBox="0 0 50 50">
					<line x1="3.18" y1="27.91" x2="17.1" y2="14" style="fill:none;stroke:#a98a13;stroke-linecap:round;stroke-miterlimit:10;stroke-width:4.75px"/>
					<line x1="3.18" y1="27.91" x2="17.1" y2="41.82" style="fill:none;stroke:#a98a13;stroke-linecap:round;stroke-miterlimit:10;stroke-width:4.75px"/>
				</svg>
				<div class="cs-arrow-body"></div>
			</div>
			
			<div class="cs-arrow next">
				<svg viewBox="0 0 50 50">
					<line x1="3.18" y1="27.91" x2="17.1" y2="14" style="fill:none;stroke:#a98a13;stroke-linecap:round;stroke-miterlimit:10;stroke-width:4.75px"/>
					<line x1="3.18" y1="27.91" x2="17.1" y2="41.82" style="fill:none;stroke:#a98a13;stroke-linecap:round;stroke-miterlimit:10;stroke-width:4.75px"/>
				</svg>
				<div class="cs-arrow-body"></div>
			</div>
		</div>
		<div>
			
		</div>
	</div><!-- Activity Wrap -->
	
	<article class="cs-summary">
		<div class="cs-navbar"> 
			<div class="cs-arrow">
				<svg viewBox="0 0 50 50">
					<line x1="3.18" y1="27.91" x2="17.1" y2="14" style="fill:none;stroke:#ffffff;stroke-linecap:round;stroke-miterlimit:10;stroke-width:4.75px"/>
					<line x1="3.18" y1="27.91" x2="17.1" y2="41.82" style="fill:none;stroke:#ffffff;stroke-linecap:round;stroke-miterlimit:10;stroke-width:4.75px"/>
				</svg>
				<div class="cs-arrow-body"></div>
			</div>
			<h1>
				Detail
			</h1>
		</div>
		<div class="cs-copy">
			<div class="cs-logo">
				<img src="<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/png/olier-brewing-co-logo.png');?>" />
			</div>
				<h3 class="cs-header">Oliver Brewing Co.</h3>
				<h4 class="cs-subheader">Tap Handles</h4>
				<div class="cs-text"> 
					<p>Steve Jones, took over as brew master of Oliver Brewering Co. 20 years ago, and has been cranking out British style ale ever since. For Steve, the artwork for his beer is more than a way of distinguishing one flavor from another, it is how he tells people about the men behind every delicious pint. So, in 2013 Oliver Brewing Co. partnered with Joshua Uzzell to introduce customers to the brew masters using cartoons and caricatures.<br><br><a class="cs-read-more" href="http://joshuauzzell.com/oliver-breweries">LEARN MORE</a></p>
				</div>
				
				
		</div>
	</article>

</div>