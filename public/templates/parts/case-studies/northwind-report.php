<?php 
$arrow = '
<div class="cs-arrow">
	<svg viewBox="0 0 50 50">
		<line x1="3.18" y1="27.91" x2="17.1" y2="14" style="fill:none;stroke:#ffffff;stroke-linecap:round;stroke-miterlimit:10;stroke-width:4.75px"/>
		<line x1="3.18" y1="27.91" x2="17.1" y2="41.82" style="fill:none;stroke:#ffffff;stroke-linecap:round;stroke-miterlimit:10;stroke-width:4.75px"/>
	</svg>
	<div class="cs-arrow-body"></div>
</div>
';	
?>
<div class="case-study row" id="northwind">
	<div class="row horizontal-line headline"><p class="header">Interactive Demo</p></div>
	<div class="activity-wrap active">
		<div class="left-nav">
			<div class="item"><p class="title">Northwind Trading Co.<br>Product Report<br><span style="font-size: 18px;font-weight: 400;"><em>Data Visualization</em></span></p></div>
			<div class="item return-to-start">
				<div class="icon">
					<img src="<?php print($paths["images"]["layout"].'case-studies/icon_return-to-start.svg');?>" />
				</div>
				<p>Return to Start</p>
			</div>
		</div>
	</div>
	
	<!-- Activity Wrap -->
	<div class="stage">
		<div class="views"> 
			<div class="view active" id="northwind-insights">
				<h2>Sample Insights</h2>
				<ul>
					<li class="insight" data-insight="1"><?php echo $arrow; ?><p>What products are in stock?</p></li>
					<li class="insight" data-insight="4"><?php echo $arrow; ?><p>What are your top sellers?</p></li>					
					<li class="insight" data-insight="2"><?php echo $arrow; ?><p>Who are your top suppliers?</p></li>
					<li class="insight" data-insight="3"><?php echo $arrow; ?><p>Where do you ship to the most?</p></li>

				</ul>
			</div>
			<div class="view" id="northwind-data">
				<h2 id="northwind-view-title">Sample View</h2>
				<div class="group" id="northwind-query-controls"></div>
				<div class="group" id="northwind-query-output"></div> 
				<div class="group" id="northwind-paging-controls"></div>
			</div>
		</div>
	</div>
	<!-- end activity ---> 
	
	<article class="cs-summary">
		<div class="cs-navbar"> 
			<div class="cs-arrow">
				<svg viewBox="0 0 50 50">
					<line x1="3.18" y1="27.91" x2="17.1" y2="14" style="fill:none;stroke:#ffffff;stroke-linecap:round;stroke-miterlimit:10;stroke-width:4.75px"/>
					<line x1="3.18" y1="27.91" x2="17.1" y2="41.82" style="fill:none;stroke:#ffffff;stroke-linecap:round;stroke-miterlimit:10;stroke-width:4.75px"/>
				</svg>
				<div class="cs-arrow-body"></div>
			</div>
			<h1>
				Detail
			</h1>
		</div>
		<div class="cs-copy">
			<div class="cs-logo">
				<img src="<?php print($paths["images"]["layout"].'case-studies/oliver-brewing-co/png/olier-brewing-co-logo.png');?>" />
			</div>
				<h3 class="cs-header">Oliver Brewing Co.</h3>
				<h4 class="cs-subheader">Tap Handles</h4>
				<div class="cs-text"> 
					<p>Steve Jones, took over as brew master of Oliver Brewering Co. 20 years ago, and has been cranking out British style ale ever since. For Steve, the artwork for his beer is more than a way of distinguishing one flavor from another, it is how he tells people about the men behind every delicious pint. So, in 2013 Oliver Brewing Co. partnered with Joshua Uzzell to introduce customers to the brew masters using cartoons and caricatures.<br><br><a class="cs-read-more" href="http://joshuauzzell.com/oliver-breweries">LEARN MORE</a></p>
				</div>
				
				
		</div>
	</article>

</div>