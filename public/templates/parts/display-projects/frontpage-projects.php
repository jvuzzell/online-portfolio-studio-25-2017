<?php
	require_once(LIBRARY_PATH.'project-profiles.php');
	
	switch($display_project_type){ 
		case 'print': 
		foreach ($project_profiles as $project) {
			if($project['project-type'] == 'project-type-print' && $project["published"] == "published"){ 
		 		include(TEMPLATES_PATH.'parts/display-projects/project-card.php');
			} 
		}
		break;
		
		case 'web': 
		foreach ($project_profiles as $project) {
			if($project['project-type'] == 'project-type-interactive' && $project["published"] == "published"){ 
		 		include(TEMPLATES_PATH.'parts/display-projects/project-card.php');
			} 
		}
		break;
				
		case 'identity': 
		foreach ($project_profiles as $project) {
			if($project['project-type'] == 'project-type-branding' && $project["published"] == "published"){ 
		 		include(TEMPLATES_PATH.'parts/display-projects/project-card.php');
			} 
		}
		break;
		
		// Default set of projects to show visitors when they first arrive
		default:
		foreach ($project_profiles as $project) {
			if($project['project-type'] == 'project-type-interactive' && $project["published"] == "published"){ 
		 		include(TEMPLATES_PATH.'parts/display-projects/project-card.php');
			} 
		}
		break;
	}	
?>