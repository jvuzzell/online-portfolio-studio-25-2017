<?php
	$client = str_replace(" ", "-", $project['client']);
	$title = str_replace(" ", "-", $project['title']);
	//$url = strtolower($home_url."/project/".$client."/".$title."/".$project['id']);
	$url = $project['slug'];
	echo "
		<a class='listed project-link col-xs-6 col-sm-4 ".$project['project-type']."' href='".$url."'>
			<section class='featured-project'>
				<div class='img-container'>
					<img src=".$project['featImg']." alt='".$project['title']."' />		
				</div>
				<div class='txt-container'>
					<div class='title'><h6>" . $project['title'] . "</h6></div>
					<p class='project-category'>" . $project['categories'][0] ."</p>
				</div>
			</section>
		</a>";
?>
