<?php
$displayNum = 6; 
$count = 0;

echo" <div class='row more-projects-container'>
		<div class='col-xs-12 col-sm-10 col-md-8 more-projects-wrap'>
			<div class='hidden-md hidden-lg' style='color: #aaa;'><h5 style='text-align: center;'>More Projects</h5></div>
	";
		
include(TEMPLATES_PATH."parts/navigation/inline-project-nav.php");
echo"	
			<div id='project-here'>";
	
			foreach ($project_profiles as $project) {
				if($project['project-type'] === $singleProject['project-type'] && $project['published'] === "published"){ 
					include(TEMPLATES_PATH.'parts/display-projects/project-card.php');
					
					$count++; 
					if($count == $displayNum){ 
						break;
					}		
				} 
			}	
	
echo"
			</div>		
		</div>	
	</div>
	"; 
	
?>