	<main class="maincontent">
		<section class="contact-me">

			
			    <fieldset class="row">
			    	<form id="contact" name="contact" method="post">
				    	<div class="col-xs-12 sender">
				    		<div class="row">
						    	<div class="col-xs-9">
						        	<input type="text" name="name" id="name" size="30" required placeholder="Name">
						    	
						        	<input type="text" name="email" id="email" size="30" required placeholder="Email">
						    	</div>
						    	<div class="col-xs-3" id="contact-insignia">
					    			<img src="<?php print_r($paths['images']['layout'].'joshua-uzzell-logo-gray.png');?>" alt="Joshua Uzzell Graphic Designer"/>
					    		</div>
				    		</div>
				    	</div>
				    	<div class="col-xs-12">
					    	<div class="row">
						    	<div class="col-xs-12">
						       		<input type="text" name="subject" id="subject" size="30" placeholder="Subject">
						    	</div>
						    	<div class="col-xs-12">
						        	<textarea name="message" id="message" required placeholder="Message"></textarea>
						    	</div>
						    	
						    	<div class="col-xs-12">
							        <label for="Answer">The Earth revolves around the... 
							        </label>
						    	</div>
						    	<div class="col-xs-12">
						        	<input type="text" name="answer" id="answer" required placeholder="*Answer"></br>
						        </div>
						        <div class="col-xs-12" style="margin-top:25px;">
						        	<input class="custom-button md-button" id="submit" type="submit" name="submit_msg" value="Send Message">
						        </div>
						    </div>
					    </div>
				    </form>
			    </fieldset>
			
			
			<div id="success">
			    <span class="textcenter">
			        <p>Your message was sent successfully!
			        <br><br>Would you like to continue browsing my <a href="/print">projects?</a>
			    </span>
			</div>
			
			<div id="error">
			    <span>
			        <p>Something went wrong, try refreshing and submitting the form again.
			        <br><br>
			        Would you like to try again? <a href="contact">Contact</a>
			        Or view the projects again? <a href="projects">Work</a></p>
			    </span>
			</div>
		</section>
	</main>	




