<?php
function displayLiveLinks($singleProject, $classes){
	$string = "";
	if ($singleProject['live-site'] === true){
		if($singleProject['multiple-urls'] === true){
			$string .= "<h5 class='".$classes." mobile-center-text'>Live Sites</h5>";
			foreach($singleProject['live-url'] as $site => $url){
				$string .= "
				<p class='visit-site ".$classes."'  style='margin: 0 0 10px 0;'>
					<a href='".$url."' target='blank' class='custom-button'>".$site."</a>
				</p>";
			}
		} else {
			$string .= "
			<p class='visit-site ".$classes."'  style='margin-bottom:1px;'>
				<a href='".$singleProject['live-url']."' target='blank' class='custom-button'>Live Site</a>
			</p>";
		}
	}
	return $string;
}
?>
<?php
	echo" <div class='interactive-wrap'>
			<div class='col-xs-12 col-sm-10 single-project'>
				<div class='row interactive-projects'>";
	echo"			<div class='col-xs-12 col-md-5 col-lg-6 hidden-xs hidden-sm'>";

						foreach ($singleProject['images'] as $image){
							echo '<img src="'.$image.'"/>';
						}
	echo"       	 </div>";


	echo"			<div class='interactive-description col-xs-12 col-md-7 col-lg-6'>
						<header class='project-name'>
							<h4 class='title'>".$singleProject['title']."</h4>
							<h5 class='subtitle'>".$singleProject['client']."</h5>
						</header>
						<p>".$singleProject['process']."</p>";

						echo displayLiveLinks($singleProject, "hidden-xs hidden-sm");

						// if ($singleProject['git'] == true){
						// 	echo"
						// 	<p class='visit-site hidden-xs hidden-sm' style='margin-top:1px;'>
						// 		<a href='".$singleProject['git-url']."' target='blank' class='custom-button'>Git Repo</a>
						// 	</p>";
						// }

	echo"
						<br/>
						<p class='role'>
							<b>Role: </b>
							<span class='text-muted'>";

								$roles="";
								foreach ($singleProject['roles'] as $role){
									$roles .= $role.", ";
								}
								$roles = substr($roles, 0, -2);
								echo $roles;

	echo"
							</span><br/>
							<b>Tech: </b>
							<span class='text-muted'>";

							$tools="";
							foreach ($singleProject['tools'] as $tool){
								$tools .= $tool.", ";
							}
							$tools = substr($tools, 0, -2);
							echo $tools;


	echo"
							</span>
						</p>
					</div>";
	echo"			<div class='col-xs-12 col-md-6 hidden-md hidden-lg' >";

						foreach ($singleProject['images'] as $image){
							echo '<img src="'.$image.'"/>';
						}

	echo"				<div style='margin-top:35px;'>";
						echo displayLiveLinks($singleProject, "");
						if ($singleProject['git'] == true){
							echo"
								<p class='visit-site' style='margin-top:1px; margin-bottom:50px;'>
									<a href='".$singleProject['git-url']."' target='blank' class='custom-button'>Git Repo</a>
								</p>";
						}
	echo"       	 	</div>";
	echo"       	 </div>";


	echo"			</div>
			<hr>
		</div>
	</div>";

?>
