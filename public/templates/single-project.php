<?php 	
	$singleProject = $project_profiles[$id];
	
	include(LIBRARY_PATH.'single-project-nav.php');
	include(TEMPLATES_PATH.'parts/navigation/single-project-nav.php');

	echo "<main class='row'>
	";
	
	if($project_profiles[$id]['project-type'] == "project-type-interactive"){ 
		include(TEMPLATES_PATH.'single-interactive.php');
	} else { 
		include(TEMPLATES_PATH.'single-design.php');	
	}
	echo "</main>";
	include(TEMPLATES_PATH.'parts/display-projects/more-projects.php');	
?>
		
