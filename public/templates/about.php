	<main class="maincontent row">
		<section class="about-me col-xs-12 col-sm-8 col-lg-6">
			<img src="<?php print_r($paths['images']['layout'].'self-portrait-joshua-uzzell.png');?>" alt="joshua uzzell graphic designer"/>

			<div class="spacer">&nbsp;</div>
			<article class="text">
				<h6 style="margin-bottom: 50px;">Background</h6>
				<p>I'm a self taught  web developer, illustrator, and graphic designer.</p>
				<p>I work at Mission Media in Baltimore, Maryland programming web apps for companies Dreamworks, The Cordish Company, and STX. </p>
				<p>I'm always down to grab coffee, talk about the future, and technology. If you are too then let's chat.</p>
			</article>
		</section>
	</main>	