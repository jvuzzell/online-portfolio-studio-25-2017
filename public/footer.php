<?php $date = new DateTime("now", new DateTimeZone('US/EASTERN')); ?>

<div class="row horizontal-line">&nbsp;</div>
	<footer class="row">
		<section id="footer-container" class="row">
			<div class="col-xs-12 col-sm-2 col-lg-1" id="logo-footer">
				<a href="<?php echo $home_url; ?>"><img  src="<?php echo $paths['images']['layout'].'joshua-uzzell-logo.png'; ?>" alt="Joshua Uzzell Graphic Designer"/></a>
			</div>
			<div class="col-xs-12 col-sm-3" id="address">
				<ul>
					<li><strong>Joshua Uzzell</strong></li>
					<li style="margin-top:15px;">Baltimore, Maryland</li>
					<li>josh.uzzell@gmail.com</li>
					<li>(202)-403-4494</li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-1 col-lg-1" id="footer-nav">
				<ul>
					<li><a href="<?php print_r($home_url);?>">Home</a></li>
					<li><a href="<?php print_r($home_url.'/about');?>">About</a></li>
					<li><a href="<?php print_r($home_url.'/resume');?>">Resume</a></li>
					<li><a href="http://www.github.com/jvuzzell">Github</a>
					<li><a href="<?php print_r($home_url.'/contact');?>">Contact</a></li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-2 col-lg-2" id="social-links">
				<ul>
				
				<li><a class="media-link" href="http://www.linkedin.com/in/joshuauzzell" target="_blank">Linkedin</a></li>
					<li><a class="media-link" href="https://www.facebook.com/Joshua.Uzzell.GraphicArtist" target="_blank">Facebook</a></li>
					<li><a class="media-link" href="https://twitter.com/joshuzzell" target="_blank">Twitter</a></li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-4 col-lg-4" id="description">
				<p>Interactive Developer, Joshua Uzzell lives in Baltimore, Maryland and builds apps for the internet. Excelling at extending brand interaction online his expertise includes PHP, HTML, CSS, Javascript and database engineering.</p>
				<p>&nbsp;</p>
				<p id="copyright">All rights reserved &#169; <?php echo $date->format('Y') ?>. <br/>
				Custom site design and development by <a href="<?php echo $home_url; ?>">Joshua Uzzell</a></p>
	
			</div>
		</section>
	</footer>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
<script type="text/javascript" src="<?php print_r($base_url.'app/assets/src/js/jquery.validate.js'); ?>"></script>
<script type="text/javascript" src="<?php print_r($base_url.'app/assets/src/js/contact-me.js'); ?>"></script>
<script type="text/javascript" src="<?php print_r($base_url.'app/assets/src/js/scripts.js'); ?>"></script>
<script type="text/javascript" src="<?php print_r($base_url.'app/assets/src/js/case-studies.js'); ?>"></script>
<script type="text/javascript" src="<?php print_r($base_url.'app/assets/src/js/case-study-oliver-brewing.js'); ?>"></script>
<?php 
if(
	$_SERVER["REMOTE_ADDR"] == "73.212.172.179" ||
	$_SERVER["REMOTE_ADDR"] == "50.193.134.9"
){
?>
<script type="text/javascript" src="<?php print_r($base_url.'app/assets/src/js/case-study-northwind-report.js'); ?>"></script>
<script type="text/javascript" src="<?php print_r($base_url.'app/assets/src/js/case-study-northwind-tables.js'); ?>"></script>
<?php 
} ?>
</body>

</html>