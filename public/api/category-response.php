<?php
$catType = $_GET['catType'];
$featured = $_GET['featured'];

include($_SERVER["DOCUMENT_ROOT"]."/app/library/project-profiles.php");
	if(!empty($catType)){
		foreach ($project_profiles as $project) {
			if($project['project-type'] == $catType && $project["published"] == "published"){
				include($_SERVER["DOCUMENT_ROOT"].'/templates/parts/display-projects/project-card.php');
			}
		}
	} elseif(!empty($featured)) {
		foreach ($project_profiles as $project) {
			if($project['featured'] == $featured && $project["published"] == "published"){
				include($_SERVER["DOCUMENT_ROOT"].'/templates/parts/display-projects/project-card.php');
			}
		}
	}
?>
