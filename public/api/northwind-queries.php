<?php error_reporting(E_ALL); ?>
<?php
class SYSTEM_REPORTER {
	PRIVATE STATIC $_instance = null;
	
	PUBLIC STATIC function Instance() {
        if (self::$_instance == null) {
            self::$_instance = new SYSTEM_REPORTER();
        }
        return self::$_instance;
    }
    
	PRIVATE $report = array(
			"reporting" => array(
				"status" => "", 
				"msg" => ""
			), 
			"data" => ""
		);
		
	PUBLIC function reporting($status, $msg, $data){
		$this->report["reporting"]["status"] = $status;
		$this->report["reporting"]["msg"] = $msg;
		$this->report["data"] = $data;
		
		return $this->report;
	}
	
	PUBLIC function output($status, $response){
		// $status not implemented 
		$response =  json_encode($response, JSON_PRETTY_PRINT);
		print_r($response);
		
		die();
	}
}

class AJAX_REQUEST {
	PUBLIC function retrieve_request(){

		try{
			if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
			    throw new Exception('Request method must be POST!');
			}
		
			$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
			if(strcasecmp($contentType, 'application/json') != 0){
			    throw new Exception('Content type must be: application/json');
			}
		
			$content = trim(file_get_contents("php://input"));
			$decoded = json_decode($content, true);
			
			if(!is_array($decoded)){
			    throw new Exception('Received content contained invalid JSON!');
			}
			
			return $decoded; 
			
		} catch( Exception $e) {
			$report = SYSTEM_REPORTER::Instance()->reporting("failure", $e, null);
			SYSTEM_REPORTER::Instance()->output("error", $report);
		}
	}
	
	
	PUBLIC function outputData($data){
		// code process further
		$report = SYSTEM_REPORTER::Instance()->reporting("success", "Data retrieval successful", $data);
		SYSTEM_REPORTER::Instance()->output("success", $report);
	}
}

class northwind_db{
	PRIVATE $dbconn = FALSE; 
	
	PRIVATE function sql_conn ($access){
		$connection_string = "dbname=joshuauz_northwind host=localhost user=joshuauz password=Movin526!";
	    
	    if($access == "open"){
		    if(!$this->dbconn){
			    $dbconn = pg_connect($connection_string);
			    if(!$dbconn){
					$report = SYSTEM_REPORTER::Instance()->reporting("failure", "Database connection not established", null);
					SYSTEM_REPORTER::Instance()->output("error", $report);
			    } else {
				    $this->dbconn = $dbconn;
			    }
		    } 
	    } else if($access == "close"){
		    pg_close($this->dbconn);
	    }
	}
	
	PUBLIC function build_product_query($params){
		// EXAMPLE: $query = SELECT product_id, product_name, product_price, $params["sort"], $params["refine"] FROM `view_name`
		$query = "SELECT";
		$query .= " product_id, product_name, product_price";
		
		$supplier_columns = "supplier_name, supplier_address, supplier_city, supplier_region, supplier_country, supplier_postalcode, supplier_phone";
		$ship_to_columns = "shipper_name, shipper_phone, order_city, order_region, order_country, order_address"; 
		
		switch($params["sort"]){
			case "product_category" :
				$query .= ", product_category";
			break;
			case "in_stock" : 
				$query .= ", in_stock";
			break; 
			case "order_count" :
				$query .= ", order_count";
			break;
			case "suppliers": 
				$query .= ", ".$supplier_columns;
			break;  
			case "shipments": 
				$query .= ", ".$ship_to_columns;
			break;
			case "city" :
				$query .= ", ".$supplier_columns; 
			break;
			case "region" : 
				$query .= ", ".$supplier_columns; 
			break; 
			case "country" : 
				$query .= ", ".$supplier_columns; 
			break; 
		}
		
		switch($params["refine"]){
			case "product_category" :
				$query .= ", product_category";
			break;
			case "stock" : 
				$query .= ", in_stock";
			break; 
			case "order_count" :
				$query .= ", order_count";
			break; 
			case "city" :
				$query .= ", ".$supplier_columns; 
			break;
			case "region" : 
				$query .= ", ".$supplier_columns; 
			break; 
			case "country" : 
				$query .= ", ".$supplier_columns; 
			break; 
		}
		
		switch($params["by"]) {
			case "product_category" : 
				$query .= " product_category";
				$query .= ' FROM "products by supplier"';
			break; 
			
			case "in_stock" : 
				$query .= ", in_stock";
				$query .= ' FROM "products by supplier"';
			break;
			
			default :
				$query .= ", order_id, order_date";
				$query .= ' FROM "products by '.$params["by"].'"';
			break;
		}
		return $query;
	}

	PUBLIC function fetch_data($query){
		$this->sql_conn("open");
		$results = pg_query($query);
		$data_assoc = "";
		if(pg_num_rows($results) > 0){
			$data_assoc = pg_fetch_all($results);
			$this->sql_conn("close");
			return $data_assoc;
		} else {
			$this->sql_conn("close");
			$report = SYSTEM_REPORTER::Instance()->reporting("warning", "No records found", null);
			SYSTEM_REPORTER::Instance()->output("error", $report);
		}
	}
} 

/*** client logic ***/
$AJAX = new AJAX_REQUEST();
$northwind_data = new northwind_db();

$request = $AJAX->retrieve_request();	

if($request["subject"] == "products") {
	$product_query = $northwind_data->build_product_query($request);
	$data = $northwind_data->fetch_data($product_query);
	$AJAX->outputData($data);
} else {
	$report = SYSTEM_REPORTER::Instance()->reporting("failure", "Invalid or missing request parameters", null);
	SYSTEM_REPORTER::Instance()->output("error", $report);
}
?>