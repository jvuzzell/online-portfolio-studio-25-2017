<?php error_reporting(E_ALL); ?>
<?php
class AJAX_REQUEST {
	PUBLIC function retrieve_request(){
		$system_response = array(
			"reporting" => array(
				"msg" => "", 
				"code" => ""
			), 
			"data" => ""
		);
		 
		try{
			if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
			    throw new Exception('Request method must be POST!');
			}
		
			$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
			if(strcasecmp($contentType, 'application/json') != 0){
			    throw new Exception('Content type must be: application/json');
			}
		
			$content = trim(file_get_contents("php://input"));
			$decoded = json_decode($content, true);
		
			if(!is_array($decoded)){
			    throw new Exception('Received content contained invalid JSON!');
			}
			
			// do something if no exception thrown by this point.
			$system_response["data"] = $decoded;
			
		} catch( Exception $e) {
			$system_response["reporting"]["msg"] = $e;
			$system_response["reporting"]["code"] = "failure"; 
		}
		
		return $system_response; 
	}
	
	PUBLIC function response($response){
		var_dump($response);
		// ultimately echo data response as json
	}
}

class northwind_db{
	PRIVATE function access (){
		
	}
	PRIVATE function determine_view(){ 
		
	}
	PUBLIC function retrieve_view($request_data){
		var_dump($request_data);
	}
	PUBLIC function publish_view(){
		
	}
} 

/*** client logic ***/
$AJAX = new AJAX_REQUEST();
$northwind_data = new northwind_db();

$callback = function(){
	return "closure";
};

$request = $AJAX->retrieve_request();
$northwind_data->retrieve_view($request["data"]);

//$AJAX->response($system_response);




?>