$(document).ready(function(){
	// GENERAL CASE STUDY CONTROLS	
	$(".cs-navbar").on("click", function(){
		var $thisSummary = $(this).parent(".cs-summary");
		var $thisActivity = $(this).parent(".cs-summary").siblings(".activity-wrap");
	
		$thisSummary.toggleClass("active");
		$thisActivity.toggleClass("active");
		$(".cs-summary").removeClass("mouse-over");
		
		$(".cs-navbar .cs-arrow").addClass("no_time");
		setTimeout(function(){$(".cs-navbar .cs-arrow").removeClass("no_time");},300);
		
	});

	$(".cs-navbar").mouseenter(function(){
		$(this).parent(".cs-summary").addClass("mouse-over");
	});
	$(".cs-summary").mouseleave(function(){
		$(".cs-summary").removeClass("mouse-over");
	});	
});