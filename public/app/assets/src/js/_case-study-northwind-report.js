(function(){
	/*** Return to Start Button ***/
	var northwind_restart_button = selectClassWithinId("northwind","return-to-start");
	var northwind_views = selectClassWithinId("northwind","view");
	var insights_view = document.getElementById("northwind-insights");
	var data_view = document.getElementById("northwind-data");

	northwind_restart_button[0].onclick = function(){
		for(var i = 0; i < northwind_views.length; i++){
			northwind_views[i].className = northwind_views[i].className.replace(/\b active\b/g,'');
		}	
		insights_view.className = insights_view.className.replace(/\b hide\b/g,'') + " active";
	};
	
	/*** Sample Insights ***/
	var sample_insight_buttons = document.getElementsByClassName("insight");
	
	for(var i = 0; i < sample_insight_buttons.length; i++){
		sample_insight_buttons[i].onclick = function(){
			var data_insight_num = this.getAttribute("data-insight"); 
			var insight_params = NorthwindViewParams.sampleParams(data_insight_num);
			var insight_data = "";
			var loadQueueName = "compilingBusinessInsight";
			
			NorthwindLoadScreen.queue(loadQueueName,"Compiling insight request");
			NorthwindQuery.requestData(insight_params.query_view, function(){
				insights_view.className = insights_view.className.replace(/\b active\b/,'');
				data_view.className = data_view.className + " active";	

				if(insight_params.view_type == "map"){
					//
				} else if(insight_params.view_type == "chart"){
					//
				} else {
					NorthwindTableView.displayBuild(NorthwindBasicTable);
				}
				
				NorthwindLoadScreen.unqueue(loadQueueName);	
			});
			
		};			
	}
	
	/*** Utility ***/
	function selectClassWithinId(id_name, class_name){
		return document.getElementById(id_name).getElementsByClassName(class_name);
	}
})();

/** Modules **/
/*
	NorthwindSystemReport
	NorthwindLoadScreen
	NorthwindQuery
	NorthwindViewParams 
	NorthwindTableView
		- NorthwindBasciProductTable
*/ 
/*************/

var NorthwindSystemReport = (function(){
	var _report = {
		"status" : null, 
		"msg" : null
	};
	
	function reporter(status, msg, multi_msg){
		_report.status = status;
		_report.msg = msg;
		
		if(multi_msg){
			_report.status = "mixed";
			_report.msg = _report.msg + "<br>" + msg;
		}
	}
	
	function output(){
		// find .northwind-message div update class and html 
		console.log(_report);
		
		// reset _report
		_report.status = null;
		_report.msg = null;
	}
	
	return {
		reporter : reporter,	
		output : output
	};
})(); // End NorthwindSystemReport

var NorthwindLoadScreen = (function(){
	var _queue = {};
	var _active = false;
	
	function _displayLoadScreen(){
		if(_stillLoading()){
			// find .northwind-loader div update class
			// display _queue[Object.keys(_queue)[0]]
			
			var queueLength = Object.keys(_queue).length;
			var queueMessage = _queue[Object.keys(_queue)[queueLength - 1]]["msg"];
			
			console.log("loader: " + queueMessage);

		} else {
			// find .northwind-loader div update class
			console.log("done loading");
		}
	}
	function _stillLoading(){
		_active = (Object.keys(_queue).length === 0) ? false : true;
		return _active; 
	}
	function queue(funcName, msg){
		_queue[funcName] = { 
			"msg" : msg
		};	
		_active = true;
		_displayLoadScreen();
	}
	function unqueue(funcName){
		delete _queue[funcName];
		_displayLoadScreen();
	}
	
	return {
		queue : queue,
		unqueue : unqueue
	};	
})(); // End Module NorthwindLoadScreen

var NorthwindQuery = (function(){
	var _storedData = [];
	var _currentQuery = {};
	
	function _outputJSON(dataStr){
		var json = JSON.parse(dataStr);
		return json;
	}
	function _storeData(queryData){
		_storedData = queryData;
	}
	function _sameQuery(query){
		var same = false;
		if(Object.keys(_currentQuery).length !== 0){
			for (var prop in query){
				if(query[prop] !== _currentQuery[prop]){
					same = false; break;
				}
			}	
		} 
		return same;
	}
	function useData(){
		return _storedData;
	}
	function requestData(query_view, callback){
		var loadQueueName = "insightDataRequest";
		var xhttp = new XMLHttpRequest();
		var sameQuery = false; 
		
		NorthwindLoadScreen.queue(loadQueueName,"Requesting insight data");
		sameQuery = _sameQuery(query_view);
		if(sameQuery === true){
			NorthwindLoadScreen.unqueue(loadQueueName);
			callback();
			return;
		} else {
			_currentQuery = query_view;
		}
		
		xhttp.open("POST", "studio-25/api/northwind-queries.php", true);
		xhttp.setRequestHeader("Content-Type", "application/json");
		xhttp.send(JSON.stringify(query_view));	

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4) {
				if(this.status == 200){
					jsonData = _outputJSON(this.responseText);
					// console.log(jsonData.reporting.status);
					// console.log(jsonData.reporting.msg);
					_storeData(jsonData.data);  
					NorthwindLoadScreen.unqueue(loadQueueName);
					callback();
					return true;
				} else {
					console.log("Error", xhttp.statusText);
					NorthwindLoadScreen.unqueue(loadQueueName);
					return false;
				}
			}
		};
	}
	return {
		useData : useData,
		requestData : requestData
	};
})(); // End Module NorthwindQuery

var NorthwindViewParams = (function(){
	function _defineView(a, b, c, d){
		var view = {
			"subject" : (a === "") ? null : a, 
			"by" : (b === "") ? null : b, 
			"sort" : (c === "") ? null : c,   
			"refine" : (d === "") ? null : d	
		};
		return view;
	}

	function sampleParams(insight_num){
		var which_insight = parseInt(insight_num);
		var view_type = "";
		var view = "";
		
		switch(which_insight){
			case 1 :  
				view_type = "table";
				view = _defineView("products","in_stock", null, null);
			break;
			
			case 2 : 
				view_type = "table";
				view = _defineView("products","ordered", "suppliers", null);
			break;
			
			case 3 : 
				view_type = "table";
				view = _defineView("products", "ordered", "shipments", null);	
			break;
			
			case 4 :
				view_type = "table";
				view = _defineView("products","ordered", null, null);
			break;
		}
		return {
			"view_type" : view_type, 
			"query_view" : view	
		};
	}
			
	return {
		sampleParams : sampleParams
	};
})(); // End Module NorthwindViewParams

var NorthwindTableView = (function(){
	function _output(loadQueueName, tableType){
		document.getElementById("northwind-query-output").innerHTML = tableType._table;
		NorthwindLoadScreen.unqueue(loadQueueName);
	}
	function displayBuild(tableType){
		var loadQueueName = "buildInsightTable";
		NorthwindLoadScreen.queue(loadQueueName, "Building table to display data");
		
		var arrOfDataObjects = NorthwindQuery.useData(); 
		
		if (arrOfDataObjects.length > 0) {
			tableType._tableHeaderArr = tableType._buildTableHeader(Object.keys(arrOfDataObjects[0]));
			tableType._tableRowsArr = tableType._buildTableRows(arrOfDataObjects);
			tableType._table = tableType._buildTable(tableType._tableHeaderArr, tableType._tableRowsArr);
			_output(loadQueueName, tableType);	
		}
	}
	return {
		displayBuild : displayBuild
	};
})(); // End Module NorthwindTableViews

var NorthwindBasicTable = (function(){
	var _tableHeaderArr = [];
	var _tableRowsArr = [];
	var _table = "";
	
	function _buildTableHeader(columnNamesArr){
		var headerArr = [];
		for(var i = 0; i < columnNamesArr.length; i++){
			headerArr[i] = "<th>" + columnNamesArr[i] +"</th>";	
		}
		
		return headerArr;
	}
	function _buildTableRows(arrOfDataObjects){
		var rows = [];
		for(var i = 0; i < arrOfDataObjects.length; i++){
			var n = 0;
			rows[i] = [];
			for(var column in arrOfDataObjects[i]){
				rows[i][n] = "<td>" + arrOfDataObjects[i][column] + "</td>";
				n++;
			}
		}
	
		return rows;
	}
	function _buildTable(headerArr, rowArr){
		var table = "<table>";
		table += "<tr>"; 
		for(var i = 0; i < headerArr.length; i++){
			table += headerArr[i];
		}
		table += "</tr>";
		for(i = 0; i < rowArr.length; i++){
			table += "<tr>"; 
			for(var n = 0; n < rowArr[i].length; n++){
				table += rowArr[i][n];
			}
			table += "</tr>";
		}
		table += "</table>";
		
		return table; 
	}
	return {
		_buildTableHeader : _buildTableHeader, 
		_buildTableRows : _buildTableRows, 
		_buildTable : _buildTable, 
		_tableHeaderArr : _tableHeaderArr, 
		_tableRowsArr : _tableRowsArr, 
		_table : _table
	};
})();