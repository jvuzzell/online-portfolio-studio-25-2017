var JSHelper = (function(){
	function selectClassInID(id_name, class_name){
		return document.getElementById(id_name).getElementsByClassName(class_name);
	}
	function selectID(id_name){
		return document.getElementById(id_name);
	}
	function setActive(elemCollection, elem, dataType){
		if(dataType == "array"){
			for(var i = 0; i < elemCollection.length; i++){
				elemCollection[i].className = elemCollection[i].className.replace(/\b active\b/g,'');
			}			
		} else if(dataType == "individual") {
			elemCollection.className = elemCollection.className.replace(/\b active\b/g,'');
		}
		elem.className = elem.className.replace(/\b hide\b/g,'') + " active";
	}
	function addClass(elemCollection, classesToAdd, dataType){
		if(dataType == "array"){
			for(var i = 0; i < elemCollection.length; i++){
				elemCollection[i].className = elemCollection[i].className + " " + classesToAdd;
			}			
		} else if(dataType == "individual") {
			elemCollection.className = elemCollection.className + " " + classesToAdd;
		}
		
	}
	function removeClass(elemCollection, classesToRemove, dataType){
		var removeClass = new RegExp(classesToRemove,'g');
		
		if(dataType == "array"){
			for(var i = 0; i < elemCollection.length; i++){
				elemCollection[i].className = elemCollection[i].className.replace(removeClass,'');
			}			
		} else if(dataType == "individual") {
			elemCollection.className = elemCollection.className.replace(removeClass,'');
		}
	}
	function hasClass(elem, classesToFind){
		var findThese = new RegExp(classesToFind,'g');
		return findThese.test(elem.className);
	}
	return { 
		selectClassInID : selectClassInID, 
		selectID : selectID,
		setActive : setActive, 
		addClass : addClass, 
		removeClass : removeClass, 
		hasClass : hasClass
	};
})();
(function(){
	/*** Return to Start Button ***/
	var northwind_restart_button = JSHelper.selectClassInID("northwind","return-to-start");
	var northwind_views = JSHelper.selectClassInID("northwind","view");
	var insights_view = document.getElementById("northwind-insights");
	var data_view = document.getElementById("northwind-data");

	northwind_restart_button[0].onclick = function(){
		JSHelper.setActive(northwind_views, insights_view, "array");
	};
	
	/*** Sample Insights ***/
	var sample_insight_buttons = document.getElementsByClassName("insight");
	
	for(var i = 0; i < sample_insight_buttons.length; i++){
		sample_insight_buttons[i].onclick = function(){
			var data_insight_num = this.getAttribute("data-insight"); 
			var insight_params = NorthwindViewParams.sampleParams(data_insight_num);
			var insight_data = "";
			var loadQueueName = "compilingBusinessInsight";
			
			NorthwindLoadScreen.queue(loadQueueName,"Compiling insight request");
			NorthwindQuery.requestData(insight_params.query_view, function(){
				JSHelper.setActive(insights_view, data_view, "individual");

				if(insight_params.view_type == "map"){
					//
				} else if(insight_params.view_type == "chart"){
					//
				} else {
					NorthwindTableView.displayBuild(NorthwindBasicTable);
				}
				
				NorthwindLoadScreen.unqueue(loadQueueName);	
			});
			
		};			
	}
	
	
})();

/** Modules **/
/*
	NorthwindSystemReport
	NorthwindLoadScreen
	NorthwindQuery
	NorthwindViewParams 
	NorthwindTableView
		- NorthwindBasciProductTable
*/ 
/*************/

var NorthwindSystemReport = (function(){
	var _report = {
		"status" : null, 
		"msg" : null
	};
	
	function reporter(status, msg, multi_msg){
		_report.status = status;
		_report.msg = msg;
		
		if(multi_msg){
			_report.status = "mixed";
			_report.msg = _report.msg + "<br>" + msg;
		}
	}
	
	function output(){
		// find .northwind-message div update class and html 
		console.log(_report);
		
		// reset _report
		_report.status = null;
		_report.msg = null;
	}
	
	return {
		reporter : reporter,	
		output : output
	};
})(); // End NorthwindSystemReport

var NorthwindLoadScreen = (function(){
	var _queue = {};
	var _active = false;
	
	function _displayLoadScreen(){
		if(_stillLoading()){
			// find .northwind-loader div update class
			// display _queue[Object.keys(_queue)[0]]
			
			var queueLength = Object.keys(_queue).length;
			var queueMessage = _queue[Object.keys(_queue)[queueLength - 1]]["msg"];
			
			console.log("loader: " + queueMessage);

		} else {
			// find .northwind-loader div update class
			console.log("done loading");
		}
	}
	function _stillLoading(){
		_active = (Object.keys(_queue).length === 0) ? false : true;
		return _active; 
	}
	function queue(funcName, msg){
		_queue[funcName] = { 
			"msg" : msg
		};	
		_active = true;
		_displayLoadScreen();
	}
	function unqueue(funcName){
		delete _queue[funcName];
		_displayLoadScreen();
	}
	
	return {
		queue : queue,
		unqueue : unqueue
	};	
})(); // End Module NorthwindLoadScreen

var NorthwindQuery = (function(){
	var _storedData = [];
	var _currentQuery = {};
	
	function _outputJSON(dataStr){
		var json = JSON.parse(dataStr);
		return json;
	}
	function _storeData(queryData){
		_storedData = queryData;
	}
	function _sameQuery(query){
		var same = false;
		if(Object.keys(_currentQuery).length !== 0){
			for (var prop in query){
				if(query[prop] !== _currentQuery[prop]){
					same = false; break;
				}
			}	
		} 
		return same;
	}
	function useData(){
		return _storedData;
	}
	function _clearData(){
		_storedData = [];
	}
	function requestData(query_view, callback){
		var loadQueueName = "insightDataRequest";
		var xhttp = new XMLHttpRequest();
		var sameQuery = false; 
		
		NorthwindLoadScreen.queue(loadQueueName,"Requesting insight data");
		_clearData();
/*
		sameQuery = _sameQuery(query_view);
		if(sameQuery === true){
			NorthwindLoadScreen.unqueue(loadQueueName);
			callback();
			return;
		} else {
			_currentQuery = query_view;
		}
*/
		
		xhttp.open("POST", "studio-25/api/northwind-queries.php", true);
		xhttp.setRequestHeader("Content-Type", "application/json");
		xhttp.send(JSON.stringify(query_view));	
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4) {
				if(this.status == 200){
					jsonData = _outputJSON(this.responseText);
					// console.log(jsonData.reporting.status);
					// console.log(jsonData.reporting.msg);
					_storeData(jsonData.data);  
					NorthwindLoadScreen.unqueue(loadQueueName);
					callback();
					return true;
				} else {
					console.log("Error", xhttp.statusText);
					NorthwindLoadScreen.unqueue(loadQueueName);
					return false;
				}
			}
		};
	}
	return {
		useData : useData,
		requestData : requestData
	};
})(); // End Module NorthwindQuery

var NorthwindViewParams = (function(){
	function _defineView(a, b, c, d){
		var view = {
			"subject" : (a === "") ? null : a, 
			"by" : (b === "") ? null : b, 
			"sort" : (c === "") ? null : c,   
			"refine" : (d === "") ? null : d	
		};
		return view;
	}

	function sampleParams(insight_num){
		var which_insight = parseInt(insight_num);
		var view_type = "";
		var view = "";
		
		switch(which_insight){
			case 1 :  
				view_type = "table";
				view = _defineView("products","in_stock", null, null);
			break;
			
			case 2 : 
				view_type = "table";
				view = _defineView("products","ordered", "suppliers", null);
			break;
			
			case 3 : 
				view_type = "table";
				view = _defineView("products", "ordered", "shipments", null);	
			break;
			
			case 4 :
				view_type = "table";
				view = _defineView("products","ordered", null, null);
			break;
		}
		return {
			"view_type" : view_type, 
			"query_view" : view	
		};
	}
			
	return {
		sampleParams : sampleParams
	};
})(); // End Module NorthwindViewParams