$(document).ready(function(){
	/*** TESTIMONIALS ***/

	endorsementTabs();

	////////////////////////////////////////
	/*** GET PROJECTS BY CATEGORY ***/
	////////////////////////////////////////

	// Hide the sidebar because If you don't have
	// Javascript turned on then the sidebar won't work.
	$(".mysidebar").removeClass("hidden");
	$(".mysidebar").addClass("col-md-2");
	$(".projects").addClass("col-md-10");

	var onlyInteractives = $("#only-interactives");
	var onlyPrint = $("#only-print");
	var onlyBranding = $("#only-branding");

	// Set this to match the initial set of projects
	// view upon arrival
	$(".frontpage #only-interactives").addClass("highlight-cat");

	$("#only-interactives, #only-print, #only-branding").on("click", function(e){
		e.preventDefault();
		var category = $(this).attr("id");

		var projectType;
		if(category == "only-interactives"){
			projectType = "project-type-interactive";
		} else if (category == "only-print"){
			projectType = "project-type-print";
		} else if (category == "only-branding"){
			projectType = "project-type-branding";
		}

		$('.highlight-cat').removeClass('highlight-cat');
		$("#"+category+"").addClass("highlight-cat");

		callProjects(projectType, "project-here");
	});

	function callProjects(projectType, displayHere){
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
			  document.getElementById(displayHere).innerHTML = xhttp.responseText;
			}
		};

		xhttp.open("GET", "api/category-response.php?catType="+projectType, true);
		xhttp.send();
	}
	////////////////////////////////////////
	/*** SLIDER EFFECT ***/
	////////////////////////////////////////
	function endorsementTabs(){

		$("#endorsement-tabs .switch").on("click", function(){
			var nextTab = $(this).data("id"),
				prevTab = $(this).parent().siblings(".active").children("a").data("id");

			console.log(prevTab, nextTab);
			$(this).parent().addClass("active");
			$(this).parent().siblings(".active").removeClass("active");

			$("#endorsement-tabs .tab[data-id="+prevTab+"]").removeClass("active");
			$("#endorsement-tabs .tab[data-id="+nextTab+"]").addClass("active");
		});
	}

	////////////////////////////////////////
	/*** LIGHTBOXING PROJECTS ***/
	////////////////////////////////////////

	$("#shade").on("click", closeLightbox1);

	window.onpopstate = function(e){
	    if(e.state){
	        document.getElementById("response").innerHTML = e.state.html;
	        document.title = e.state.pageTitle;
	    }
	}

/*
	var projectLinks = document.getElementsByClassName("project-link");

	for (var i = 0; i < projectLinks.length; i++){
		projectLinks[i].addEventListener("click", function(event){
			var urlPath,
				slug;

			event.preventDefault();

			var slug = this.getAttribute("href");
			slug = slug.substr(1,slug.length);

			url = "api/project-response.php?slug="+slug;
			loadDoc(url, slug, activateLightbox);

		});
	}
*/

	function loadDoc(url, slug, cFunction) {
		var xhttp;

		xhttp=new XMLHttpRequest();
		xhttp.responseType = "json";
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
			  cFunction(slug, this.response);
			}
		};

		xhttp.open("GET", url, true);
		xhttp.send();
	}

	function activateLightbox(projectSlug, projectJSON) {
		openLightbox1();

	    document.title = response.pageTitle;
	    window.history.pushState({},"", historyURL);
	}

	function openLightbox1(){
		$("#lightbox1, #shade").addClass("active");
	}

	function closeLightbox1(){
		$("#lightbox1, #shade").removeClass("active");

	}

});
