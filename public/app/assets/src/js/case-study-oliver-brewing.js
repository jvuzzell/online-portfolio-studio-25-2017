$(document).ready(function(){
	$("#oliverbrew .cs-navbar").on("click", function(){
		if($(this).parent(".cs-summary").hasClass("active") === true){
			$(".ollie-right-hand").css("opacity", 0.4);
		} else {
			$(".ollie-right-hand").css("opacity",1.0);
		}
			
	});
	
	// OLIVER BREWING - RESTART
	$("#oliverbrew .return-to-start").on("click", function(){
		
		$("#oliverbrew .taps").attr("data-view", "start");
		removePosition();
		
		$(".tap[data-tap='1'], .tap[data-tap='2'], .tap[data-tap='3']").addClass("position-3");
		$(".tap[data-tap='4'], .tap[data-tap='5'], .tap[data-tap='6']").addClass("position-1");
		
		resetTaps();
		$(".tap-names").removeClass("active").addClass("hidden");	
	
		$("#oliverbrew .ollie, .ollie-right-hand").removeClass("off-screen");
		$("#oliverbrew .ollie").addClass("active");

	});
	
	// OLIVER BREWING - DETAIL VIEW
	$("#oliverbrew .tap").on("click", function(){activateTapDetail(this)});
	$("#oliverbrew .tap-name").on("click", function(){activateTapDetail(this)});
	
	$("#oliverbrew .cs-slider .cs-arrow.next").on("click", function(){slideTaps("next")});
	$("#oliverbrew .cs-slider .cs-arrow.previous").on("click", function(){slideTaps("previous")});

	// OLIVER BREWING - GRID VIEW 
	$("#oliverbrew .grid-view").on("click", function(){
		if(($("#oliverbrew .taps").attr("data-view")) == "start" || ($("#oliverbrew .taps").attr("data-view")) == "detail"){
			$("#oliverbrew .ollie, .ollie-right-hand").removeClass("active").addClass("off-screen");
		
			$("#oliverbrew .taps").attr("data-view", "grid");
			$(".case-study .left-nav .item").addClass("lock-pointer");
			$(".tap[data-tap='4'], .tap[data-tap='5'], .tap[data-tap='6']").addClass("fade-in");
			setTimeout(function(){
				removePosition();
				$(".tap").addClass("position-2");
				$(".case-study .left-nav .item").removeClass("lock-pointer");
				mouseOverShiftSiblings(".tap.position-2", "shift-focus");
				mouseOverHighlightTap(".tap.position-2", ".tap-name", "active");
				mouseOverHighlightTap(".tap.position-2", ".tap.position-2", "active");
				mouseOverHighlightTap(".tap-name", ".tap.position-2", "active");
				$(".tap-name").removeClass("lock-pointer");
			}, 500);
		
		} 	
		resetTaps();
		$(".tap-names").removeClass("hidden").addClass("active");
	});
});

function activateTapDetail(elem) {
	if($("#oliverbrew .ollie, .ollie-right-hand").hasClass("off-screen") == false){ 
		$("#oliverbrew .ollie, .ollie-right-hand").addClass("off-screen");
	}
	$("#oliverbrew .taps").attr("data-view", "detail");
	removePosition();
	
	
	$(".tap-labels").addClass("active");
	$(".tap-label").removeClass("fade-in");
	var tapToActivate = $(elem).attr("data-tap")
	var numTapToActivate = parseInt(tapToActivate);
	
	$(".cs-slider").addClass("active");
	activateTap(numTapToActivate);
	$(".tap-names").removeClass("hidden").addClass("active");
	
	
	lockTapName(tapToActivate);
}

function activeTapName(tapNum){
	$(".tap-name").removeClass("active");
	
	if(tapNum == undefined || tapNum == ""){ 
		tapNum = "1"; 
	} 
	
	$(".tap-name[data-tap='"+tapNum+"']").addClass("active");	
}

function slideTaps(direction){ 
	var activeTapNum = parseInt($("#oliverbrew .cs-slider").attr("data-slide"));
	
	numTaps = $(".tap").length;

	if(direction == "next"){ 
		var nextTapNum = activeTapNum + 1;
		if(nextTapNum > numTaps){ 
			nextTapNum = 1;
		}
		tapToActivate = nextTapNum;
		
	} else if(direction == "previous"){ 
		var prevTapNum = activeTapNum - 1; 
		if(prevTapNum < 1){ 
			prevTapNum = numTaps;
		}
		tapToActivate = prevTapNum; 
	}

	activateTap(tapToActivate);
	
	strTapToActivate = tapToActivate.toString();
	lockTapName(strTapToActivate);
}

function activateTap(nextTapNum){ 
	removePosition();

	$(".tap").each(function(index, element){
		var currentTapNum = parseInt($(element).attr("data-tap"));
		nextTapNum 		  = parseInt(nextTapNum);
		
		if(currentTapNum < nextTapNum){
			$(element).addClass("position-6 lock-pointer slide-transitions");
		}
		
		else if(currentTapNum == nextTapNum){
			var active_tap = nextTapNum.toString();
			$(element).addClass("position-5 lock-pointer slide-transitions");
			$("#oliverbrew .cs-slider").attr("data-slide", nextTapNum);
			setTimeout(function(){
				$(".tap-label").removeClass("fade-in");
				$(".tap-label[data-label='"+active_tap+"']").addClass("fade-in");
				activeTapName(active_tap);
			}, 400);
		} 
		
		else if(currentTapNum > nextTapNum){
			$(element).addClass("position-4 lock-pointer slide-transitions");
		}
	});
	
	if($(".tap[data-tap='1']").hasClass("position-5")){
		$(".tap[data-tap='6']").removeClass("position-4"); 
		$(".tap[data-tap='6']").addClass("position-6");
		
	} else if($(".tap[data-tap='6']").hasClass("position-5")){ 
		$(".tap[data-tap='1']").removeClass("position-6");
		$(".tap[data-tap='1']").addClass("position-4");

	}
}

function resetTaps(){ 
	$(".tap-labels").removeClass("active");
	$(".tap-label").removeClass("fade-in");
	$(".tap-name").removeClass("active");
	$(".taps .tap").removeClass("lock-pointer slide-transitions");
	$(".cs-slider").removeClass("active");
}

function removePosition(){
	$(".tap").removeClass(function(index, className){ 
		var classesToRemove = [];
		class_name = className.substr(className.indexOf("position"), className.length + 1);
		
		return class_name;
	});
}

function mouseOverShiftSiblings(selector, activeClass){ 
	$(selector).mouseenter(function(){
		$(this).siblings(selector).addClass(activeClass);
	});
	$(selector).mouseleave(function(){
		$(this).siblings(selector).removeClass(activeClass);
	});	
}

function mouseOverHighlightTap(selector, affectedElem, activeClass){ 
	if(selector == ".tap-name"){ 
		$(selector).mouseenter(function(){
			var tapNum = $(this).attr("data-tap");
			$(affectedElem+"[data-tap='"+tapNum+"'").addClass(activeClass).siblings(".tap.position-2").addClass("shift-focus");
			
		});
		$(selector).mouseleave(function(){
			var tapNum = $(this).attr("data-tap");
			
			if(".taps[data-view='grid']"){ 
				$(affectedElem+"[data-tap='"+tapNum+"'").removeClass(activeClass).siblings(".tap.position-2").removeClass("shift-focus");
			}
		});
	} else {
		$(selector).mouseenter(function(){
			var tapNum = $(this).attr("data-tap");
			$(affectedElem+"[data-tap='"+tapNum+"'").addClass(activeClass);
		});
		$(selector).mouseleave(function(){
			var tapNum = $(this).attr("data-tap");
			$(affectedElem+"[data-tap='"+tapNum+"'").removeClass(activeClass);
		});
	}

}

function lockTapName(tapToLock){ 
	$(".tap-name").removeClass("lock-pointer");
	$(".tap-name[data-tap='"+tapToLock+"']").addClass("lock-pointer");
}
