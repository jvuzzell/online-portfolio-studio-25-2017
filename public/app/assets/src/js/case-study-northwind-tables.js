/** Module Extensions  **/
/*
	NorthwindTableView
		- NorthwindBascicTable
		- NorthwindBascicTable
*/ 
/*************/
var NorthwindTableView = (function(){
	function _cleanArray(actual){
		var newArray = new Array();
		for (var i = 0; i < actual.length; i++) {
			if (actual[i]) {
				newArray.push(actual[i]);
			}
		}
		return newArray;		
	}
	function _pagination(tableType, headerArr, rowArr){
		var loadQueueName = "_pagination";
		NorthwindLoadScreen.queue(loadQueueName, "Generating booklet from tables");
		
		var	pages = [];
		var	pageNum = 0; 
		var	perPage = 12;
		var	rowCount = 0;
		var	rowTotal = rowArr.length;
		var r = 0,  
			i = 0,
			n = 0; 
		pages[pageNum] = []; 
		
		for(r = 0; r < rowTotal; r++){
			pages[pageNum][r] = [];
			pages[pageNum][r] = rowArr[rowCount];
			i++;
			
			if(i == perPage){
				i = 0;
				pageNum++;
				pages[pageNum] = [];
			} 
			rowCount++; 
		}	
			
		for(n = 0; n < pages.length - 1; n++){
			page = _cleanArray(pages[n]);
			tableType._dataBooklet[n] = [];
			tableType._dataBooklet[n] = tableType._buildPage(headerArr, page, ("page-"+n));
		}
	
		NorthwindLoadScreen.unqueue(loadQueueName);
	}
	function _page_numbers(tableType){
		var totalPageDisplayNumbers = 6;
		var northwind_paging_controls = document.getElementById("northwind-paging-controls");
		var allPageNumHTMLstrArr = [];
		var page_controls = prependPageCount(0);
		var totalPageCount = tableType._dataBooklet.length; 
		
		page_controls += "<ul class='page-numbers'>";
		if(totalPageCount > totalPageDisplayNumbers){page_controls += appendRewindButton();}
		for(var i = 0; i < tableType._dataBooklet.length; i++){
			allPageNumHTMLstrArr[i] = "<li><a class='page-number' data-page='"+i+"'>"+ (i+1) +"</a></li>";
			if(i < totalPageDisplayNumbers){
				page_controls += allPageNumHTMLstrArr[i];
			}
		}
		if(totalPageCount > totalPageDisplayNumbers){
			page_controls += appendFastFowardButton();
			page_controls += appendPageSearchField();
		}
		page_controls += "</ul>";
		
		_output(northwind_paging_controls, page_controls);
		updateDisplayedPageNumbers(0);
		
		// View Controls
		var pages = document.getElementsByClassName("northwind-table-page");
		JSHelper.addClass(pages[0], "active", "individual");
		pageNumberEventListeners();
		
		function _output(container, HTMLstring){
			container.innerHTML = ""; 
			container.innerHTML = HTMLstring;
		}
		function pageNumberEventListeners(){
			var HTMLArr = JSHelper.selectClassInID("northwind","page-number");
			var searchPageField = JSHelper.selectID("northwind-search-page-field");
			var directionalButtonArr = JSHelper.selectClassInID("northwind", "northwind-table-page-nav");
			var n = 0, 
				x = 0, 
				y = 0;

			if(HTMLArr.length > 0){
				for(n = 0; n < HTMLArr.length; n++){
					HTMLArr[n].onclick = function(){
						var pageNum = this.getAttribute("data-page");
						changePage(this, pageNum);	
						updateDisplayedPageNumbers(pageNum);	
					};
				}				
			}
			if(searchPageField !== null){
				var searchButton = JSHelper.selectID("northwind-page-search-button"); 
				searchButton.onclick = function(){
					if(JSHelper.hasClass(searchButton, "valid")){
						var num = (parseInt(searchPageField.value) - 1);
						changePage(HTMLArr[(totalPageDisplayNumbers/2)], num);
						updateDisplayedPageNumbers(num);	
					} else {
						// do nothing
					}
				};
				
				searchPageField.addEventListener('input', function(){
					var input = parseInt(this.value);
					var reg = /^\d+$/;
					
					if(reg.test(input)){
						if(input > 0 && input <= totalPageCount){
							validSearch();
						} else {
							invalidSearch();
						}
					} else {
						invalidSearch();
					}
					
					function validSearch(){
						JSHelper.removeClass(searchButton, "invalid", 'individual');
						JSHelper.addClass(searchButton, "valid", 'individual');	
					}
					function invalidSearch(){
						JSHelper.removeClass(searchButton, "valid", 'individual');
						JSHelper.addClass(searchButton, "invalid", 'individual');
					}
				});
			}
			//class='northwind-table-page-nav' data-table-direction='rewind'
			if(directionalButtonArr.length > 0){
				for(y = 0; y < directionalButtonArr.length; y++){
					directionalButtonArr[y].onclick = function(){
						var direction = this.getAttribute("data-table-direction");
						if(direction == "rewind"){
							changePage(HTMLArr[0], 0);
							updateDisplayedPageNumbers(0);
						} else if(direction == "forward"){
							updateDisplayedPageNumbers(totalPageCount-1);
							changePage(HTMLArr[totalPageDisplayNumbers], totalPageCount-1);
						}
					};
				}
			}	
		}
		function changePage(pageHTMLelem, pageNum){
			var page = JSHelper.selectClassInID("northwind","page-"+pageNum);
			var HTMLArr = JSHelper.selectClassInID("northwind","page-number");
			console.log(pageHTMLelem);
			JSHelper.setActive(pages, page[0], "array");
			JSHelper.setActive(HTMLArr, pageHTMLelem, "array");
		}
		function updateDisplayedPageNumbers(currentPageNum){
			var currentPageNum = parseInt(currentPageNum);
			var prevCounter = currentPageNum - 1; 
			var nextCounter = currentPageNum + 1;
			var diff = 0; 
			var reverseIndex = (totalPageDisplayNumbers/2) - 1;
			var pageNumToDisplay = {
				prev : [], 
				next : []	
			};
			var pageNumbers = JSHelper.selectClassInID("northwind", "page-number");
			var page_controls = "";
			var x = 0, 
				p = 0, 
				i = 0, 
				n = 0, 
				y = 0, 
				f = 0;

			page_controls = prependPageCount(currentPageNum);
			page_controls += "<ul class='page-numbers'>";
			if(totalPageCount > totalPageDisplayNumbers){page_controls += appendRewindButton();}	
					
			if(totalPageCount <= totalPageDisplayNumbers){
				for (x = 0; x < totalPageCount; x++){ 
					page_controls += "<li><a class='page-number' data-page='" + x + "'>"+ (x + 1) +"</a></li>";
				}
			} else if(currentPageNum <= (totalPageDisplayNumbers/2)){
				for (x = 0; x < (totalPageDisplayNumbers + 1); x++){ 
					page_controls += "<li><a class='page-number' data-page='" + x + "'>"+ (x + 1) +"</a></li>";
				}
			} else if((totalPageCount - currentPageNum) < totalPageDisplayNumbers){
				var counter = totalPageCount - (totalPageDisplayNumbers + 1); 
				for (p = 0; p < (totalPageDisplayNumbers + 1); p++){ 
					page_controls += "<li><a class='page-number' data-page='" + counter + "'>"+ (counter + 1) +"</a></li>";
					counter++;
				}
			} else {
				for(i = 0; i < (totalPageDisplayNumbers/2); i++){
					pageNumToDisplay.prev[reverseIndex] = prevCounter--;
					pageNumToDisplay.next[i] = nextCounter++;
					reverseIndex--;
				}
				
				// previous
				for (n = 0; n < pageNumToDisplay.prev.length; n++){ 
					page_controls += "<li><a class='page-number' data-page='" + pageNumToDisplay.prev[n] + "'>"+ (pageNumToDisplay.prev[n]+1) +"</a></li>";
				}
				
				// current
				page_controls += "<li><a class='page-number' data-page='"+ currentPageNum +"'>"+ (currentPageNum + 1) +"</a></li>";
				
				// next
				for (y = 0; y < pageNumToDisplay.next.length; y++){ 
					page_controls += "<li><a class='page-number' data-page='"+ pageNumToDisplay.next[y] +"'>"+ (pageNumToDisplay.next[y]+1) +"</a></li>";
				}
			}
			if(totalPageCount > totalPageDisplayNumbers){
				page_controls += appendFastFowardButton();
				page_controls += appendPageSearchField();
			}
			page_controls += "</ul>";
			
			_output(northwind_paging_controls, page_controls);
			pageNumberEventListeners();
			
			for(f = 0; f < pageNumbers.length; f++){
				if(pageNumbers[f].getAttribute("data-page") == currentPageNum){
					pageNumElem = pageNumbers[f];
				}
			}
			JSHelper.setActive(pageNumbers, pageNumElem, "array");
		}
		function appendRewindButton(){
			return "<li class='northwind-directional-page-nav'><a class='northwind-table-page-nav' data-table-direction='rewind'><img class='rewind' src='/app/assets/img/layout/case-studies/icon_rewind.svg'></a></li>";
		}
		function appendFastFowardButton(){
			return "<li class='northwind-directional-page-nav'><a class='northwind-table-page-nav' data-table-direction='forward'><img class='forward' src='/app/assets/img/layout/case-studies/icon_rewind.svg'></a></li>";
		}
		function prependPageCount(pN){
			return "<div id='northwind-page-count'><p>" + (pN + 1) + " of " + tableType._dataBooklet.length + "</p></div>";
		}
		function appendPageSearchField(){
			return "<li id='northwind-page-search'><input type='text' name='northwind-search-page-field' id='northwind-search-page-field'><a id='northwind-page-search-button' class='invalid'></a></li>";
		}
	}
	function _output(loadQueueName, tableType){
		var northwind_output_div = document.getElementById("northwind-query-output");
		northwind_output_div.innerHTML = "";
		
		for(var i = 0; i < tableType._dataBooklet.length; i++){
			northwind_output_div.innerHTML += tableType._dataBooklet[i];	
		}
		
		_page_numbers(tableType);
		NorthwindLoadScreen.unqueue(loadQueueName);
	}
	function displayBuild(tableType){
		var loadQueueName = "displayBuild";
		tableType._dataBooklet = [];
		
		NorthwindLoadScreen.queue(loadQueueName, "Compiling data into tables");
		
		var arrOfDataObjects = NorthwindQuery.useData(); 
		
		if (arrOfDataObjects.length > 0) {
			tableType._tableHeaderArr = tableType._buildTableHeader(Object.keys(arrOfDataObjects[0]));
			tableType._tableRowsArr = tableType._buildTableRows(arrOfDataObjects);
			//tableType._table = tableType._buildTable(tableType._tableHeaderArr, tableType._tableRowsArr);
			_pagination(tableType, tableType._tableHeaderArr, tableType._tableRowsArr);
			_output(loadQueueName, tableType);	
		}
	}
	return {
		displayBuild : displayBuild
	};
})(); // End Module NorthwindTableViews

var NorthwindBasicTable = (function(){
	var _tableHeaderArr = [];
	var _tableRowsArr = [];
	var _dataBooklet = [];
	
	function _columnName(str){
		str = str.replace(/[\_]/g, " ");
		str = str.replace(/\w\S*/g, function(str){return str.charAt(0).toUpperCase() + str.substr(1).toLowerCase();});
		return str;
	}
	function _buildTableHeader(columnNamesArr){
		var headerArr = [];
		for(var i = 0; i < columnNamesArr.length; i++){
			headerArr[i] = "<th>" + _columnName(columnNamesArr[i]) +"</th>";	
		}
		
		return headerArr;
	}
	function _buildTableRows(arrOfDataObjects){
		var rows = [];
		for(var i = 0; i < arrOfDataObjects.length; i++){
			var n = 0;
			rows[i] = [];
			for(var column in arrOfDataObjects[i]){
				rows[i][n] = "<td>" + arrOfDataObjects[i][column] + "</td>";
				n++;
			}
		}
	
		return rows;
	}
	function _buildPage(headerArr, page, className){
		var table = "<table class='northwind-table-page "+className+"'>";
		table += "<tr>"; 
		for(var i = 0; i < headerArr.length; i++){
			table += headerArr[i];
		}
		table += "</tr>";
		for(var r = 0; r < page.length; r++){
			table += "<tr>"; 
			for(var n = 0; n < page[r].length; n++){
				table += page[r][n];
			}
			table += "</tr>";
		}
		table += "</table>";
		
		return table; 
	}
	return {
		_buildTableHeader : _buildTableHeader, 
		_buildTableRows : _buildTableRows, 
		_buildPage : _buildPage, 
		_tableHeaderArr : _tableHeaderArr, 
		_tableRowsArr : _tableRowsArr, 
		_dataBooklet : _dataBooklet
	};
})();