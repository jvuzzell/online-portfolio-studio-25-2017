(function(){
	/*** Return to Start Button ***/
	var northwind_restart_button = selectClassWithinId("northwind","return-to-start");
	var northwind_views = selectClassWithinId("northwind","view");
	var insights_view = document.getElementById("northwind-insights");
	var data_view = document.getElementById("northwind-data");

	northwind_restart_button[0].onclick = function(){
		for(var i = 0; i < northwind_views.length; i++){
			northwind_views[i].className = northwind_views[i].className.replace(/\b active\b/g,'');
		}
		insights_view.className = insights_view.className.replace(/\b hide\b/g,'') + " active";
	}

	/*** Sample Insights ***/
	var sample_insight_buttons = document.getElementsByClassName("insight");

	for(var i = 0; i < sample_insight_buttons.length; i++){
		sample_insight_buttons[i].onclick = function(){
			var data_insight_num = this.getAttribute("data-insight");
			var insight_params = NorthwindViews.InsightParams(data_insight_num);
			var insight_data = "";

			NorthwindQuery.RequestData(insight_params["query_params"], function(){
				insights_view.className = insights_view.className.replace(/\b active\b/,'');
				data_view.className = data_view.className + " active";
			});
		};
	}

	/*** Utility ***/
	function selectClassWithinId(id_name, class_name){
		return document.getElementById(id_name).getElementsByClassName(class_name);
	}
})();

var NorthwindQuery = (function(){

	return {
		RequestData : function(query_params, callback){
			var xhttp = new XMLHttpRequest();

			xhttp.open("POST", "api/northwind-queries.php", true);
			xhttp.setRequestHeader("Content-Type", "application/json");
			xhttp.send(JSON.stringify(query_params));
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4) {
					if(this.status == 200){
						console.log(this.responseText);
						callback();
						return this.responseText;
					} else {
						console.log("Error", xhttp.statusText);
						return false;
					}
				}
			};
		}
	}
})();

var NorthwindViews = (function(){

	return {
		InsightParams : function(insight_num){
			var which_insight = parseInt(insight_num);
			var view_type = "";
			var query_params = {};

			switch(which_insight){
				case 1 :
					view_type = "table";
					query_params = {
						"subject" : "products",
						"by" : "in-stock"
					};
				break;

				case 2 :
					view_type = "table";
					query_params = {
						"subject" : "products",
						"by" : "ordered"
					};
				break;

				case 3 :
					view_type = "table";
					query_params = {
						"subject" : "products",
						"by" : "ordered",
						"sort" : "suppliers"
					};
				break;

				case 4 :
					view_type = "table";
					query_params = {
						"subject" : "products",
						"by" : "ordered",
						"sort" : "shippment"
					};
				break;
			}

			return {
				"view_type" : view_type,
				"query_params" : query_params
			};
		}// end InsightParam

	}// end return
})();
