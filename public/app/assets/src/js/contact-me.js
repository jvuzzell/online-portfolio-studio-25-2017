jQuery.validator.addMethod('answercheck', function (value, element) {
        return this.optional(element) || /^\b(sun|Sun)\b$/.test(value);
    }, "That's not quite correct.");

// validate contact form
$(function() {
    $('#contact').validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },
            message: {
                required: true,
                minlength: 25
            },
            answer: {
                required: true,
                answercheck: true
            }
        },
        messages: {
            name: {
                required: "Please provide your name.",
                minlength: "Minimum length of 2 letters."
            },
            email: {
                required: "Please provide your email."
            },
            message: {
                required: "What's on your mind?",
                minlength: "Minimum length of 25 characters."
            },
            answer: {
                required: "Sorry, please try again!"
            }
        },
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                type:"POST",
                data: $(form).serialize(),
                url:"app/library/process-msg.php",
                success: function() {
                    $('#contact :input').attr('disabled', 'disabled');
                    $('#contact').fadeTo( "slow", 0, function() {
                        $(this).find(':input').attr('disabled', 'disabled');
                        $(this).find('label').css('cursor','default');
                        $('#success').fadeIn();



                    });
                },
                error: function() {
                    $('#contact').fadeTo( "slow", 0.15, function() {
                        $('#error').fadeIn();
                        $('#error').delay(1800).fadeOut(1500);
                    });
                }
            });
        }
    });
});
