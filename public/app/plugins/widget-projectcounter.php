<?php
//echo "<pre>"; print_r($projects); echo "</pre>";
$num = count($projects);
$numLogos = 0;
$numPub = 0;
$numSites = 0; 
$numPack = 0;
$numAds = 0;
$numIllst = 0;
$numUncat = 0;

foreach ($projects as $project){ 

	foreach ($project['categories'] as $categories){
		 $termA = "Logo";
		 $termB = "Publication";
		 $termC = "Digital";
		 $termD = "Package";
		 $termF = "Advertisement";
		 $termG = "Illustration";
		 $termE = "Uncategorized";
		 if($categories == $termA){
			$numLogos ++;
			}
		elseif($categories == $termB){
			$numPub ++;
			} 
		elseif($categories == $termC){
			$numSites ++;
			} 
		elseif($categories == $termD){
			$numPack ++;
			} 
		elseif($categories == $termE){ 
			$numUncat ++;
			}
		elseif($categories == $termG){ 
			$numIllst ++;
			}
		elseif($categories == $termF){ 
			$numAds ++;
			}
		}
	}
	
echo "

<div id='project-counter'>
	
	<h3 class='widget-title'>Designs</h3>
	<dl>
		<span class='category'>
			<dt class='cat-title'>Logos/Brands</dt>
			<dd class='cat-num'>" . $numLogos . "</dd>
		</span>
		
		<span class='category'>
			<dt class='cat-title'>Advertisements</dt>
			<dd class='cat-num'>" . $numAds . "</dd>
		</span>
		
		<span class='category'>
			<dt class='cat-title'>Illustrations</dt>
			<dd class='cat-num'>" . $numIllst . "</dd>
		</span>
		
		<span class='category'>		
			<dt class='cat-title'>Publication Designs</dt>
			<dd class='cat-num'>" . $numPub . "</dd>
		</span>	
		
		<span class='category'>		
			<dt class='cat-title'>Package Designs</dt>
			<dd class='cat-num'>" . $numPack . "</dd>
		</span>	
		
		<span class='category'>		
			<dt class='cat-title'>Digital Designs</dt>
			<dd class='cat-num'>" . $numSites . "</dd>
		</span>	
		
		<span class='category'>	
			<dt class='cat-title'>Total Projects</dt>
			<dd class='cat-num'>" . $num . "</dd>
		</span>
	</dl>
</div>
";
?>