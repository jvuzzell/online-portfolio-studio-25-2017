<?php
	$base_url = "http://".$_SERVER['SERVER_NAME']."/";
    $home_url = "http://".$_SERVER['SERVER_NAME'];
    $test_url = "http://".$_SERVER['SERVER_NAME']."/testing/";
    $paths = array(
        "images" => array(
            "content" => "http://". $_SERVER['SERVER_NAME'] . "/app/assets/img/content/",
            "layout" => "http://". $_SERVER['SERVER_NAME'] . "/app/assets/img/layout/",
            "uploads" => "http://". $_SERVER['SERVER_NAME'] . "/app/assets/img/uploads/",
			),
        "templates" => "http://". $_SERVER['SERVER_NAME'] . "/templates/",
        "library" => "http://". $_SERVER['SERVER_NAME'] . "/app/library/",
        "src" => "http://". $_SERVER['SERVER_NAME'] . "/app/assets/src/",
        "plugins" => "http://". $_SERVER['SERVER_NAME'] . "/app/plugins/"
    );

	define("LIBRARY_PATH", $_SERVER["DOCUMENT_ROOT"]."/app/library/");

	define("TEMPLATES_PATH", $_SERVER["DOCUMENT_ROOT"]."/templates/");

 	define("DIRECTORY_PATH", $_SERVER["DOCUMENT_ROOT"]."/");

 	define("SRC_PATH", $_SERVER["DOCUMENT_ROOT"]."/app/assets/src/");

 	define("PLUGINS_PATH", $_SERVER["DOCUMENT_ROOT"]."/app/plugins/");
?>
