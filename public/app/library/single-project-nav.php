<?php 
$curr_id = (int) $id;

$next_id = activeProject($curr_id, "next", $project_profiles);	
$prev_id = activeProject($curr_id, "prev", $project_profiles);

$next_url = $project_profiles[$next_id]["slug"];
$prev_url = $project_profiles[$prev_id]["slug"];

function iterator($num, $dir){
	if($dir == "next"){
		$num++;
	} else if ($dir == "prev"){
		$num--; 
	}	
	return $num; 	
}

function activeProject($cID, $direction, $projects){ 
	$empty = true;
	$catFound = false;
	$projectFound = false; 

	$pID = iterator($cID, $direction);

	while($projectFound === false){ 
		
		// Exceeds project id range
		if($pID > count($projects) + 1){ 
			$pID = 0;
			$empty = false; // I know that project '0' isn't empty
		
		} else if ($pID < 0){
			$pID = count($projects);
			$empty = false; // I know that project '0' isn't empty
		} 
		
		// Is empty
		if (!empty($projects[$pID]) && $projects[$pID]["published"] == "published"){ 
			$empty = false;
			
			// Is category
			if ($projects[$cID]["project-type"] == $projects[$pID]["project-type"]){
				$catFound = true;
			}
		}
		
		// project found 
		if($empty === false && $catFound === true){
			$projectFound = true;
		} else {
			$pID = iterator($pID, $direction);
		}
	}
	return ($pID);
}
?>
