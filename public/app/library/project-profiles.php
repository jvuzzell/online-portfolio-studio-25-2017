<?php
// $path = "http://". $_SERVER['SERVER_NAME'] . "/app/assets/img/uploads/";

$path = "/app/assets/img/uploads/";

$project_profiles = array(
		"0" => array(
			"id"	      =>	0,
			"published"	  =>  "published",
			"slug"        =>   "/entertainment-cruises",
			"industry"	  =>	"Hospitality",
			"title"       => 	"Seasonal Marketing Material",
			"client"      =>  	"Entertainment Cruises Headquarters in Chicago",
			"roles"		  =>	array("Graphic Designer","Production Artist"),
			"tools"       =>  	array("Adobe Indesign", "Adobe Photoshop", "Adobe Acrobat"),
			"process"     =>  	"While working at Entertainment Cruises Headquarters in Chicago, Graphic Designer Joshua Uzzell helped their national sales & marketing teams prepare for the holiday seasons.<br/><br/>Uzzell’s primary responsibility was to design brochures, menus, and digital signage for seasonal cruises, which involved photo retouching and text layout. Uzzell’s keen eye for detail was invaluable in his work as he was responsible for maintaining consistency for 7 brands that were sold in 8 U.S. cities.
",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Print"),
			"featImg"     =>  	$path."entertainment-cruises/entertainment-cruises-joshua-uzzell_feat-img.png",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."entertainment-cruises/entertainment-cruises-joshua-uzzell_7.png",
									$path."entertainment-cruises/entertainment-cruises-joshua-uzzell_6.png",
									$path."entertainment-cruises/entertainment-cruises-joshua-uzzell_1.png",
									$path."entertainment-cruises/entertainment-cruises-joshua-uzzell_2.png",
									$path."entertainment-cruises/entertainment-cruises-joshua-uzzell_3.png",
									$path."entertainment-cruises/entertainment-cruises-joshua-uzzell_4.png",
									$path."entertainment-cruises/entertainment-cruises-joshua-uzzell_5.png",

							 	),
			"project-type"	  => 	"project-type-print",
			"featured"    =>    false

 		),
		"1" => array(
			"id"	      =>	1,
			"published"	  =>  "published",
			"slug"        =>   "/pratt-street-ale-house",
			"industry"	  =>	"Hospitality",
			"title"       => 	"Promotional Signage",
			"client"      =>  	"206 Restaurant Group",
			"roles"		  =>	array("Creative Direction","Graphic Designer"),
			"tools"       =>  	array("Adobe Illustrator"),
			"process"     =>  	"Pratt Street Ale House serves craft beer from one of the oldest breweries in Baltimore. In 2011, the owners asked Graphic Designer Joshua Uzzell to design a series of ads that would inspire guests to return and bring their friends.</br></br>The ads hung in the restaurant’s main bar, which challenged Uzzell to create designs that were inspiring while unimposing. He achieved this balance by featuring visual queues that guests would find familiar from their experience at the restaurant. Examples included photos of dining spaces, artwork from the craft beer selection, and even caricatures of the brewers.",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Print"),
			"featImg"     =>  	$path."206-restaurant-group/joshua-uzzell-pratt-street-ale-house-feat-img.png",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."206-restaurant-group/joshua-uzzell_pratt-street-ale-house-ad-17.png",
									$path."206-restaurant-group/joshua-uzzell_pratt-street-ale-house-ad-08.png",
									$path."206-restaurant-group/joshua-uzzell_pratt-street-ale-house-ad-02.png",
									$path."206-restaurant-group/joshua-uzzell_pratt-street-ale-house-ad-03.png",
									$path."206-restaurant-group/joshua-uzzell_pratt-street-ale-house-ad-04.png",
									$path."206-restaurant-group/joshua-uzzell_pratt-street-ale-house-ad-05.png",
									$path."206-restaurant-group/joshua-uzzell_pratt-street-ale-house-ad-06.png"

							 	),
			"project-type"	  => 	"project-type-print",
			"featured"    =>    true
		),
		"2" => array(
			"id"	      =>	2,
			"published"	  =>  "published",
			"slug"        =>   "/columbia-ale-house",
			"industry"	  =>	"Hospitality",
			"title"       => 	"Promotional Signage",
			"client"      =>  	"206 Restaurant Group",
			"roles"		  =>	array("Creative Direction","Graphic Designer"),
			"tools"       =>  	array("Adobe Illustrator"),
			"process"     =>  	"The Ale House Columbia stands at the heart of Columbia, Maryland supplying visitors with live music, gourmet food and a vast selection of craft beer. When it opened in 2013, the owners asked Graphic Designer Joshua Uzzell to create visuals for an in-store advertising campaign that promoted seasonal events and specials.</br></br>The request required Uzzell to create graphics that could be read at a distance on a thin surface area. He overcame the challenge by focusing on visual elements that guests strongly associate with a season or particular event. This drew their attention while hinting at the nature of the ad. With part of the message implied Uzzell could concentrate on creating truly insightful callouts.",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Print"),
			"featImg"     =>  	$path."206-restaurant-group/joshua-uzzell-the-columbia-ale-house-feat-img.png",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."206-restaurant-group/joshua-uzzell_the-columbia-ale-house-08.png",
									$path."206-restaurant-group/joshua-uzzell_the-columbia-street-ale-house-15.png",
									$path."206-restaurant-group/joshua-uzzell_the-columbia-street-ale-house-16.png",
									$path."206-restaurant-group/joshua-uzzell_the-columbia-street-ale-house-17.png",

							 	),
			"project-type"	  => 	"project-type-print",
			"featured"    =>    true
		),
		"3" => array(
			"id"	  	  =>	3,
			"published"	  =>  "published",
			"slug"        =>   "/baltimore-city-part-1",
			"industry"	  =>	"Local Government",
			"title"       => 	"Half Page Magazine",
			"client"      =>  	"Baltimore City Law Department",
			"tools"       =>  	array("Adobe Illustrator"),
			"roles"		  =>	array("Creative Direction","Graphic Designer","Photographer"),
			"process"     =>  	"In 2014, the Baltimore City Law Department seized an opportunity to showcase the lesser known appeal of Baltimore City at the 2014 IMLA Conference. To help them tell their story, Graphic Designer Joshua Uzzell photographed 14 neighborhoods throughout the city and used the images to design a series of brochures for guided bus tours.</br></br>Of the series, there was a half-page magazine that Uzzell had more freedom to design. With that freedom he devised a series of layouts that featured beautiful day lit photos prudently balanced with text to create a handout that viewers would want to keep for their next visit to Baltimore.",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Print"),
			"featImg"     =>  	$path."IMLA/feat-img-baltimore-city-law-department-joshua-uzzell.png",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."IMLA/baltimore-city-neighborhood-guide-joshua-uzzell_post-image-1.png",
									$path."IMLA/baltimore-city-neighborhood-guide-joshua-uzzell_post-image-2.png",
									$path."IMLA/baltimore-city-neighborhood-guide-joshua-uzzell_post-image-3.png",
									$path."IMLA/baltimore-city-neighborhood-guide-joshua-uzzell_post-image-4.png",
									$path."IMLA/baltimore-city-neighborhood-guide-joshua-uzzell_post-image-5.png",
									$path."IMLA/baltimore-city-neighborhood-guide-joshua-uzzell_post-image-6.png",
									$path."IMLA/baltimore-city-neighborhood-guide-joshua-uzzell_post-image-7.png",
							 	),
			"project-type"	  => 	"project-type-print",
			"featured"    =>    true
		),
		"4" => array(
			"id"	  	  =>	4,
			"published"	  =>  "published",
			"slug"        =>   "/baltimore-city-part-2",
			"industry"	  =>	"Local Government",
			"title"       => 	"Brochures",
			"client"      =>  	"Baltimore City Law Department",
			"tools"       =>  	array("Adobe Illustrator"),
			"roles"		  =>	array("Creative Direction","Graphic Designer","Photographer"),
			"process"     =>  	"In 2014, the Baltimore City Law Department seized an opportunity to showcase the lesser known appeal of Baltimore City at the 2014 IMLA Conference. To help them tell their story, Graphic Designer Joshua Uzzell photographed 14 neighborhoods throughout the city and used the images to design a series of brochures for guided bus tours.</br></br>Of the series, there was a set of three brochures that covered the different tour routes through the city. Uzzell was challenged to create one layout that would compliment each experience without being entirely different.<br/><br/>To answer the challenge, Uzzell devised a color scheme that complimented the burgundy featured in the IMLA Conference logo. Each color corresponded with only one brochure and was applied to its cover and illustrations. This allowed each brochure to be very similar yet distinguishable to viewers.",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Print"),
			"featImg"     =>  	$path."IMLA/feat-img-2-baltimore-city-law-department-joshua-uzzell.png",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."IMLA/baltimore-city-neighborhood-guide-joshua-uzzell_post-image-8.png",
									$path."IMLA/baltimore-city-neighborhood-guide-joshua-uzzell_post-image-9.png",
									$path."IMLA/baltimore-city-neighborhood-guide-joshua-uzzell_post-image-10.png",
									$path."IMLA/baltimore-city-neighborhood-guide-joshua-uzzell_post-image-11.png",
							 	),
			"project-type"	  => 	"project-type-print",
			"featured"    =>    true
		),
		"5" => array(
			"id"	      =>	5,
			"published"	  =>  "published",
			"slug"        =>   "/oliver-breweries",
			"industry"	  =>	"Hospitality",
			"title"       => 	"Tap Handles",
			"client"      =>  	"Oliver Breweries",
			"tools"       =>  	array("Adobe Illustrator"),
			"roles"		  =>	array("Creative Direction","Graphic Designer"),
			"process"     =>  	"Steve Jones, took over as the brew master of Oliver Breweries 20 years ago, and has been cranking out heavy metal and British inspired beer ever since. When the brewery fell under new ownership, Jones saw this as an opportunity to reintroduce himself to craft beer fans.<br/><br/>To do this, he requested that Graphic Designer Joshua Uzzell, illustrate a series of tap handles featuring caricatures of him and his assistant. Uzzell drew inspiration from the names of beer, CD covers in Jones's collection, and pictures hanging in the brewery. The resulting series was refreshing and funny as it invited fans to share in Jones's shenanigans and inside jokes.",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Print"),
			"featImg"     =>  	$path."oliver-breweries/feat-img-tap-handle-the-spruce-is-loose-close-up.png",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."oliver-breweries/joshua-uzzell-tap-handle-if-you-want-blood.png",
									$path."oliver-breweries/joshua-uzzell-tap-handle-if-you-want-blood-close-up.png",
									$path."oliver-breweries/joshua-uzzell-tap-handle-ironman-pale-ale.png",
									$path."oliver-breweries/joshua-uzzell-tap-handle-ironman-pale-ale-close-up.png",
									$path."oliver-breweries/joshua-uzzell-tap-handle-merry-ole-ale.png",
									$path."oliver-breweries/joshua-uzzell-tap-handle-merry-ole-ale-close-up.png",
									$path."oliver-breweries/joshua-uzzell-tap-handle-the-spruce-is-loose.png",
									$path."oliver-breweries/joshua-uzzell-tap-handle-the-spruce-is-loose-close-up.png",
									$path."oliver-breweries/joshua-uzzell-tap-handle-war-1812-derek.png",
									$path."oliver-breweries/joshua-uzzell-tap-handle-war-1812-derek-close-up.png",
									$path."oliver-breweries/joshua-uzzell-tap-handle-war-1812-steve.png",
									$path."oliver-breweries/joshua-uzzell-tap-handle-war-1812-steve-close-up.png",
									$path."strongman/oliver-breweries-strongman-pale-ale-joshua-uzzell-2.png"

							 	),
			"project-type"	  => 	"project-type-print",
			"featured"    =>    true
		),
		"6" => array(
			"id"	      =>	6,
			"published"	  =>  "published",
			"slug"        =>   "/lexi-and-her-donkey",
			"industry"	  =>	"Early Childhood",
			"title"       => 	"Childrens Book",
			"client"      =>  	"The Adventures of Lexi and her Donkey",
			"tools"       =>  	array("Adobe Illustrator"),
			"roles"		  =>	array("Creative Direction","Graphic Designer"),
			"process"     =>  	"When Steve Bridges, was diagnosed with cancer he approached Graphic Designer Joshua Uzzell with a request. Jones wanted Uzzell to illustrate the children's story that he had written about his daughter and her imaginary friend.<br/><br/>Uzzell eagerly accepted the request and began working after reading Jones's draft. Storyboards, settings, and character designs were drawn by hand on separately from each other. The images were merged together and colored in Adobe Illustrator. Then they were imported into Uzzell's storybook layout prepared Adobe Indesign before printing.",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Print"),
			"featImg"     =>  	$path."steve-bridges/feat-img-the-adventures-of-lexi-and-her-donkey-joshua-uzzell.png",
			"space-images"=> 	false,
			"images"      =>  	array(
									$path."steve-bridges/1-the-adventures-of-lexi-and-her-donkey-joshua-uzzell.png",
									$path."steve-bridges/2-the-adventures-of-lexi-and-her-donkey-joshua-uzzell.png",
									$path."steve-bridges/3-the-adventures-of-lexi-and-her-donkey-joshua-uzzell.png",
									$path."steve-bridges/4-the-adventures-of-lexi-and-her-donkey-joshua-uzzell.png",
									$path."steve-bridges/5-the-adventures-of-lexi-and-her-donkey-joshua-uzzell.png",
									$path."steve-bridges/6-the-adventures-of-lexi-and-her-donkey-joshua-uzzell.png",
									$path."steve-bridges/7-the-adventures-of-lexi-and-her-donkey-joshua-uzzell.png",
									$path."steve-bridges/8-the-adventures-of-lexi-and-her-donkey-joshua-uzzell.png",

							 	),
			"project-type"	  => 	"project-type-print",
			"featured"    =>    true
		),
		"7" => array(
			"id"	      =>	7,
			"published"	  =>  "published",
			"slug"        =>   "/putnam-pantry-packaging",
			"industry"	  =>	"Novelty & Candies",
			"title"       => 	"Packaging Solutions",
			"client"      =>  	"Putnam Pantry Candies & Ice Creams",
			"roles"		  =>	array("Graphic Designer","Illustrator"),
			"tools"       =>  	array("Adobe Illustrator","Adobe Photoshop"),
			"process"     =>  	"",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Print"),
			"featImg"     =>  	$path."galeforce-consulting-partners/feat-img-putnam-pantry-joshua-uzzell-2.png",
			"space-images"=> 	false,
			"images"      =>  	array(
									$path."galeforce-consulting-partners/putnam-pantry-joshua-uzzell-01.png",
									$path."galeforce-consulting-partners/putnam-pantry-joshua-uzzell-02.png",
									$path."galeforce-consulting-partners/putnam-pantry-joshua-uzzell-03.png",
									$path."galeforce-consulting-partners/putnam-pantry-joshua-uzzell-04.png",
									$path."galeforce-consulting-partners/putnam-pantry-joshua-uzzell-05.png",


							 	),
			"project-type"	  => 	"project-type-print",
			"featured"    =>    false
		),
		"8" => array(
			"id"	      =>	8,
			"published"	  =>  "published",
			"slug"        =>   "/logo-collection-1",
			"industry"	  =>	"",
			"title"       => 	"Logo Collection Pt. 1",
			"client"      =>  	"Various Clients",
			"roles"		  =>	array("Graphic Designer","Illustrator"),
			"tools"       =>  	array("Adobe Illustrator"),
			"process"     =>  	"This is a brief selection of full color logos designed and illustrated by Graphic Designer Joshua Uzzell.",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Branding"),
			"featImg"     =>  	$path."full-color-logos/feat-img-logo-collection-01.png",
			"space-images"=> 	false,
			"images"      =>  	array(
									$path."full-color-logos/joshua-uzzell_heritage-mechanical.png",
									$path."full-color-logos/joshua-uzzell_exposed-treasures.png",
									$path."full-color-logos/joshua-uzzell_be-more-sugar.png",
									$path."full-color-logos/joshua-uzzell_putnam-pantry-candies.png",
									$path."full-color-logos/joshua-uzzell_barr.png",
									$path."full-color-logos/joshua-uzzell_the-war-of-1812.png",
									$path."full-color-logos/joshua-uzzell_lex-and-her-donkey.png",
									$path."full-color-logos/joshua-uzzell_aiga-blend.png",
									$path."full-color-logos/joshua-uzzell_aaron-j-barnett.png",
									$path."full-color-logos/joshua-uzzell_body-mind-spirit.png",
									$path."full-color-logos/joshua-uzzell_sajjas.png",


							 	),
			"project-type"	  => 	"project-type-branding",
			"featured"    =>    false
		),

		"10" => array(
			"id"	      =>	10,
			"published"	  =>  "published",
			"slug"        =>   "/body-mind-spirit-therapy",
			"industry"	  =>	"Health Care",
			"title"       => 	"Body Mind Spirit MFR Massage Therapy",
			"client"      =>  	"Emily Jacobs",
			"tools"       =>  	array("Adobe Illustrator"),
			"roles"		  =>	array("Creative Direction","Graphic Designer"),
			"process"     =>  	"Emily Jacobs is a massage therapist that specializes in Myofascial Release. It is a form of massage therapy that works by targeting tension in the connective tissues surrounding the muscle rather than the muscle itself. This indirect approach to healing chronic pain stood out to Graphic Designer Joshua Uzzell, and was decidedly important in visualizing the brand.</br></br>Combined with the fact that patients turn to Myofascial Release when medicine and traditional therapy do not work, Uzzell perceived that the treatment was associated with hope. Jacobs likened this hope to that of bumblebees which overcome their physical limitations in order to fly.</br></br>This pseudoscientific fact resonated with Uzzell, giving him a clear visual for the logo. The resulting illustration of a bumblebee doubled as a symbol for hope and an educational affect for introducing patients to Myofascial Release.",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Branding"),
			"featImg"     =>  	$path."emily-jacobs/feat-img-body-mind-spirit-mfr-joshua-uzzell.png",
			"space-images"=> 	false,
			"images"      =>  	array(
									$path."emily-jacobs/body-mind-spirit-myofascial-release-01.png",
									$path."emily-jacobs/body-mind-spirit-myofascial-release-02.png",
									$path."emily-jacobs/body-mind-spirit-myofascial-release-03.png",
							 	),
			"project-type"	  => 	"project-type-branding",
			"featured"    =>    true
		),
		"11" => array(
			"id"	      =>	11,
			"published"	  =>  "published",
			"slug"        =>   "/be-more-sugar",
			"industry"	  =>	"Baked Goods",
			"title"       => 	"Be More Sugar",
			"client"      =>  	"Danielle Lechert",
			"tools"       =>  	array("Adobe Illustrator"),
			"roles"		  =>	array("Creative Direction","Graphic Designer"),
			"process"     =>  	"To spur success and save a local business, Graphic Designer Joshua Uzzell interned with Be More Sugar to create a logo and packaging concepts to renew its appeal.<br/><br/>During the branding process, Uzzell focused on designing a color palette that was distinct amongst other bakeries. To do this he sought inspiration in Be More Sugar's cocktail-cupcake selection; which were sweet, edgy, and inherently for adults. The resulting color palette created a distinct tone for the hand-lettered logo, custom icons, and patterns he created. ",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Branding"),
			"featImg"     =>  	$path."danielle-lechert/feat-img-be-more-sugar-joshua-uzzell.png",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."danielle-lechert/1-be-more-sugar-joshua-uzzell.png",
									$path."danielle-lechert/2-be-more-sugar-joshua-uzzell.png",
									$path."danielle-lechert/3-be-more-sugar-joshua-uzzell.png",
									$path."danielle-lechert/4-be-more-sugar-joshua-uzzell.png",
									$path."danielle-lechert/5-be-more-sugar-joshua-uzzell.png",
							 	),
			"project-type"	  => 	"project-type-branding",
			"featured"    =>    false
		),

		"12" => array(
			"id"	      =>	12,
			"published"	  =>  "published",
			"slug"        =>   "/aiga-converse",
			"industry"	  =>	"Non-Profit",
			"title"       => 	"AIGA Converse",
			"client"      =>  	"AIGA Baltimore",
			"roles"		  =>	array("Creative Direction","Graphic Designer"),
			"tools"       =>  	array("Adobe Illustrator"),
			"process"     =>  	"As an organizer of AIGA Baltimore’s networking events, Joshua Uzzell worked to make each event more appealing to design professionals. To do this he updated each event's logo, strategic positioning and digital banner design.<br/><br/>For the popular roundtable discussion Converse, Uzzell decided that the original design was sound but, needed a modern touch. To achieve this he introduced a fresh color palette, and incorporated flat icons for visual context in the digital banner. Furthermore, to establish prominence within the community he repositioned the event as “Baltimore’s Open Forum for Design.”",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Branding"),
			"featImg"     =>  	$path."aiga/converse/uzzell_feat-img-aiga-converse-lettering.png",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."aiga/converse/aiga-converse-joshua-uzzell_3.png",
									$path."aiga/converse/aiga-converse-joshua-uzzell_2.png",
									$path."aiga/converse/aiga-converse-joshua-uzzell_1.png"

								 ),
			"project-type"	  => 	"project-type-branding",
			"featured"    =>    true
		),
		"13" => array(
			"id"	      =>	13,
			"published"	  =>  "published",
			"slug"        =>   "/aiga-blend",
			"industry"	  =>	"Non-Profit",
			"title"       => 	"AIGA Blend",
			"client"      =>  	"AIGA Baltimore",
			"roles"		  =>	array("Creative Direction","Graphic Designer"),
			"tools"       =>  	array("Adobe Illustrator"),
			"process"     =>  	"As an organizer of AIGA Baltimore’s networking events, Joshua Uzzell worked to make each event more appealing to design professionals. To do this he updated each event's logo, strategic positioning and digital banner design.<br/><br/>For the popular happy hour event Blend, the original design and positioning desperately needed an update. Retaining only the image of a skyline, Uzzell created a new hand lettered logo, introduced a friendlier color palette, and changed the event’s byline. The new look and feel reflected changes of how the event was hosted. No longer was Blend just a happy hour, it became a local event where professionals could “Socialize, network and learn.”
",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Branding"),
			"featImg"     =>  	$path."aiga/blend/uzzell_feat-img-aiga-blend.png",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."aiga/blend/aiga-blend-joshua-uzzell_1.png",
									$path."aiga/blend/uzzell_aiga-blend_new.png",
									$path."aiga/blend/uzzell_aiga-blend_lettering.png"
							 	),
			"project-type"	  => 	"project-type-branding",
			"featured"    =>    true
		),
		"21" => array(
			"id"	      =>	21,
			"published"	  =>  "published",
			"slug"        =>  "logo-collection-2",
			"industry"	  =>	"",
			"title"       => 	"Logo Collection Pt. 2",
			"client"      =>  	"Various Clients",
			"roles"		  =>	array("Graphic Designer","Illustrator"),
			"tools"       =>  	array("Adobe Illustrator"),
			"process"     =>  	"This is a brief selection of black and white logos designed and illustrated by Graphic Designer Joshua Uzzell.",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Branding"),
			"featImg"     =>  	$path."black-and-white-logos/feat-img-logo-collection-02.png",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."black-and-white-logos/joshua-uzzell_mama-carol.png",
									$path."black-and-white-logos/joshua-uzzell_cic-land-studios.png",
									$path."black-and-white-logos/joshua-uzzell_three-cheers.png",
									$path."black-and-white-logos/joshua-uzzell_gold-mind.png",
									$path."black-and-white-logos/joshua-uzzell_uncle-ron.png",
									$path."black-and-white-logos/joshua-uzzell_karrrma.png",
									$path."black-and-white-logos/joshua-uzzell_king-fader.png",
									$path."black-and-white-logos/joshua-uzzell_tokyo-shock.png",

							 	),
			"project-type"	  => 	"project-type-branding",
			"featured"    =>    false
		),

		"14" => array("published"	  =>  "draft"),

		/**** INTERACTIVES ****/

		"24" => array(
			"id"	      =>	24,
			"published"	  =>  "published",
			"slug"        =>   "/dreamworks-create",
			"industry"	  =>	"Entertainment",
			"title"       => 	"Dreamworks Create Online Games",
			"client"      =>  	"Mission Media - Dreamworks Create",
			"roles"		  =>	array("Web Developer"),
			"tools"       =>  	array("HTML, Javascript, JQUery, CSS3 Keyframes, SVG, Canvas"),
			"process"     =>  	"As a web developer at <a href='http://www.missionmedia.com' target='_blank' style='color: #ff501c;'>Mission Media</a>, Joshua Uzzell, led the development of four Javascript based character building apps for Trolls, How to Train Your Dragon, and Dinotrux. Each app leveraged HTML5 Canvas and Javascript exclusively to create printable PDFs for 3D models, coloring book pages, scrapbooks, and paper airplanes reflecting the users in game creation. He used modular design patterns, API integrations, and JSON data models to create a custom MVC framework that was loosely translated into each of the apps. ",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Interactive"),
			"featImg"     =>  	$path."dreamworks-create/feat-img_dreamworks-create.jpg",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."dreamworks-create/mission-media-dreamworks-create-trolls.png",
									$path."dreamworks-create/mission-media-dreamworks-create-how-to-train-your-dragon.png"

							 	),
			"project-type"	  => 	"project-type-interactive",
			"live-site"	  =>    true,
			"multiple-urls"	  =>  true,
			"live-url"	  =>    array(
									"My Troll Friend" => "http://create.dreamworks.com/trollfriend",
									"Troll Scrapbook" => "http://create.dreamworks.com/trollscrapbook",
									"How To Train Your Dragon" => "http://create.dreamworks.com/dragon-flyer",
									"Dinotrux" => "http://create.dreamworks.com/dinotrux"
								),
			"git"		  =>    false,
			"git-url"     =>    "",
			"featured"    =>    false
		),
		"23" => array(
			"id"	      =>	23,
			"published"	  =>  "published",
			"slug"        =>   "/surgeon-700",
			"industry"	  =>	"Athletics",
			"title"       => 	"Product Landing Page - STX Surgeon 700",
			"client"      =>  	"Mission Media - STX",
			"roles"		  =>	array("Web Developer"),
			"tools"       =>  	array("HTML, SASS, JQUery, CSS3 Keyframes, SVG"),
			"process"     =>  	"As a web developer at <a href='http://www.missionmedia.com' target='_blank' style='color: #ff501c;'>Mission Media</a>, Joshua Uzzell, led the production of the STX Surgeon 700 landing page. The site's visual effects and motion graphics were achieved through CSS3 Keyframes and custom SASS mixins.",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Interactive"),
			"featImg"     =>  	$path."stx-surgeon-700/feat-img_stx-surgeon-700.jpg",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."stx-surgeon-700/mission-media-stx-surgeon-700.png"

							 	),
			"project-type"	  => 	"project-type-interactive",
			"live-site"	  =>    true,
			"multiple-urls"	  =>  false,
			"live-url"	  =>    "https://www.stx.com/surgeon-700",
			"git"		  =>    false,
			"git-url"     =>    "",
			"featured"    =>    false
		),
		"22" => array(
			"id"	      =>	22,
			"published"	  =>  "published",
			"slug"        =>   "/becoming-68",
			"industry"	  =>	"Athletics",
			"title"       => 	"Product Landing Page - STX Becoming 68",
			"client"      =>  	"Mission Media - STX",
			"roles"		  =>	array("Web Developer"),
			"tools"       =>  	array("HTML, CSS, JQUery, Email, Publicaster, Litmus"),
			"process"     =>  	"As a web developer at <a href='http://www.missionmedia.com' target='_blank' style='color: #ff501c;'>Mission Media</a>, Joshua Uzzell programmed a series of HTML emails, digital banners, and a landing page to promote the periodic release of videos and photography from the would be Netflix Original, STX Becoming 68.",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Interactive"),
			"featImg"     =>  	$path."stx-becoming-68/feat-img_stx-becoming-68.jpg",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."stx-becoming-68/mission-media-stx-becoming-68-website-email.png"

							 	),
			"project-type"	  => 	"project-type-interactive",
			"live-site"	  =>    true,
			"multiple-urls"	  =>  true,
			"live-url"	  =>    array(
									"Landing Page" => "https://www.stx.com/becoming-68",
									"HTML Email" => "/mission/stx-becoming-68-email/index.html"
								),
			"git"		  =>    false,
			"git-url"     =>    "",
			"featured"    =>    false
		),
		"25" => array(
			"id"	      =>	25,
			"published"	  =>  "published",
			"slug"        =>   "/mission-media-emails",
			"industry"	  =>	"Marketing",
			"title"       => 	"HTML Email Templates",
			"client"      =>  	"Mission Media - Various",
			"roles"		  =>	array("Web Developer"),
			"tools"       =>  	array("HTML, CSS, Litmus, Publicaster"),
			"process"     =>  	"",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Interactive"),
			"featImg"     =>  	$path."mission-media-emails/feat-img_mission-media-emails.jpg",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."mission-media-emails/mission-media-casey-cares-html-email.png",
									$path."mission-media-emails/mission-media-bcdss-html-email.png",
									$path."mission-media-emails/mission-media-waterside-district-html-email.png",
									$path."mission-media-emails/mission-media-mcfaddens-sports-grill-html-email.png"
							 	),
			"project-type"	  => 	"project-type-interactive",
			"live-site"	  =>    true,
			"multiple-urls"	  =>  true,
			"live-url"	  =>    array(
									"Casey Cares" => "/mission/casey-cares-email/index.html",
									"Baltimore Department of Social Services" => "/mission/bcdss-email/index.html",
									"McFaddens Sports Saloon" => "/mission/eci-mcfaddens-email/index.html",
									"Waterside District" => "/mission/eci-waterside-district/index.html",
									"STX Becoming 68" => "/mission/stx-becoming-68-email/index.html"
								),
			"git"		  =>    false,
			"git-url"     =>    "",
			"featured"    =>    false
		),

		"26" => array(
			"id"	      =>	26,
			"published"	  =>  "published",
			"slug"        =>   "/collectors-car-corral",
			"industry"	  =>	"Automotive",
			"title"       => 	"HTML Templates for Multipage Website",
			"client"      =>  	"Mission Media - Collectors Car Corral",
			"roles"		  =>	array("Front End Developer"),
			"tools"       =>  	array("HTML", "CSS"),
			"process"     =>  	"",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Interactive"),
			"featImg"     =>  	$path."collectors-car-corral/feat-img_collectors-car-corral.jpg",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."collectors-car-corral/mission-media-collectors-car-coral.png"

							 	),
			"project-type"	  => 	"project-type-interactive",
			"live-site"	  =>    true,
			"multiple-urls"	  =>  false,
			"live-url"	  =>    "http://www.collectorscarcorral.com",
			"git"		  =>    false,
			"git-url"     =>    "",
			"featured"    =>    false
		),
		"27" => array(
			"id"	      =>	27,
			"published"	  =>  "published",
			"slug"        =>   "/everyman-theatre",
			"industry"	  =>	"Entertainment",
			"title"       => 	"HTML Templates for Drupal Site",
			"client"      =>  	"Mission Media - Everyman Theatre",
			"roles"		  =>	array("Front End Developer"),
			"tools"       =>  	array("HTML", "CSS"),
			"process"     =>  	"",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Interactive"),
			"featImg"     =>  	$path."everyman-theatre/feat-img_everyman-theatre.jpg",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."everyman-theatre/mission-media-everyman-theatre-1.png",
									$path."everyman-theatre/mission-media-everyman-theatre-3.png",
									$path."everyman-theatre/mission-media-everyman-theatre-2.png"


							 	),
			"project-type"	  => 	"project-type-interactive",
			"live-site"	  =>    true,
			"multiple-urls"	  =>  false,
			"live-url"	  =>    "http://everymantheatre.org",
			"git"		  =>    false,
			"git-url"     =>    "",
			"featured"    =>    false
		),

		"15" => array(
			"id"	      =>	15,
			"published"	  =>  "published",
			"slug"        =>   "/ishimoto-massage-therapy",
			"industry"	  =>	"Health Care",
			"title"       => 	"Mobile Responsive Website",
			"client"      =>  	"Janet Ishimoto",
			"roles"		  =>	array("Graphic Designer","Web Designer","Web Developer"),
			"tools"       =>  	array("Zurb Foundation","MySQL", "HTML", "CSS", "PHP", "Adobe Photoshop", "Adobe Illustrator"),
			"process"     =>  	"When Massage Therapist Janet Ishimoto went into business for herself she asked Graphic Designer Joshua Uzzell to help her build her brand. Understanding the foundation of Ishimoto’s business, Uzzell developed a branded starter kit that allowed Ishimoto to efficiently promote her business, manage appointments, and process clients.<br/><br/>The starter kit included a logo, business cards, letterheads, and a mobile responsive website that facilitated appointment booking, and payment processing.
",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Interactive"),
			"featImg"     =>  	$path."interactive/featured-images/feat-img_ishimoto-massage-therapy.png",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."ishimoto-mt/joshua-uzzell-ishimoto-mt-1.png",
									$path."ishimoto-mt/joshua-uzzell-ishimoto-mt-2.png"

							 	),
			"project-type"	  => 	"project-type-interactive",
			"live-site"	  =>    false,
			"multiple-urls"	  =>  false,
			"live-url"	  =>    "",
			"git"		  =>    true,
			"git-url"     =>    "",
			"featured"    =>    true
		),
		"16" => array(
			"id"	      =>	16,
			"published"	  =>  "published",
			"slug"        =>   "/putnam-pantry-website",
			"industry"	  =>	"E-commerce",
			"title"       => 	"Wordpress Website with WooCommerce Plugin",
			"client"      =>  	"Putnam Pantry Candies & Ice Cream",
			"roles"		  =>	array("Web Developer","Web Designer"),
			"tools"       =>  	array("Zurb Foundation", "MySQL", "PHP", "XHTML", "JQuery", "MAMP", "Wordpress", "Woocommerce"),
			"process"     =>  	"As an extension of the rebranding project for Putnam Pantry Candies & Ice Cream, Graphic Designer Joshua Uzzell built an online store to bring the new look and feel into the digital world.<br/><br/>The online store was built by Uzzell using a combination of Wordpress and Woocommerce plugins giving the Putnam Pantry Candies & Ice Cream a high level of control for managing their products online.
",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Interactive","Branding"),
			"featImg"     =>  	$path."interactive/featured-images/feat-img_putnam-pantry-candies.png",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."galeforce-consulting-partners/joshua-uzzell_putnam-pantry-website-1.png"

							 	),
			"project-type"	  => 	"project-type-interactive",
			"live-site"	  =>    false,
			"multiple-urls"	  =>  false,
			"live-url"	  =>    "",
			"git"		  =>    false,
			"git-url"     =>    "",
			"featured"    =>    false
		),
		"17" => array(
			"id"	      =>	17,
			"published"	  =>  "published",
			"slug"        =>   "/uzzell-portfolio",
			"industry"	  =>	"Tech",
			"title"       => 	"My Portfolio Website",
			"client"      =>  	"Personal Project",
			"roles"		  =>	array("Web Designer","Web Developer"),
			"tools"       =>  	array("Bootstrap","CSS","HTML","PHP", "Javascript", "JQuery","MySQL"),
			"process"     =>  	"
",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Interactive"),
			"featImg"     =>  	$path."my-website/feat-img_joshua-uzzell-portfolio.png",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."my-website/joshua-uzzell-portfolio-1.png",
									$path."my-website/joshua-uzzell-portfolio-2.png"

							 	),
			"project-type"	  => 	"project-type-interactive",
			"live-site"	  =>    false,
			"multiple-urls"	  =>  false,
			"live-url"	  =>    "",
			"git"		  =>    false,
			"git-url"     =>    "",
			"featured"    =>    false

		),
		"18" => array(
			"id"	      =>	18,
			"published"	  =>  "published",
			"slug"        =>   "/myhtml-email-app",
			"industry"	  =>	"Tech",
			"title"       => 	"Mail Client for HTML Emails",
			"client"      =>  	"MyHtml Email - Personal Project",
			"roles"		  =>	array("Web Developer"),
			"tools"       =>  	array("PHP", "XHTML", "JQuery"),
			"process"     =>  	"As a way to respond to an increase in jobs requiring experience with HTML emails Graphic Designer and Web Developer Joshua Uzzell, built MyHtml Email. It features three hand-coded promotional emails which Uzzell is able to personalize and send to potential employers. Sending each email is quick and easy as MyHTML Email interface only requires that the user input a proper email address, subject, and a message then press send. <br/><br/>Additionally, MyHTML Email supports carbon copy (cc), blind carbon copy (bcc), saving drafts, and backups of sent messages. It is a great way to look professional while being personal online.
",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Interactive"),
			"featImg"     =>  	$path."myhtml-email/feat-img_html-email-templates.png",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."myhtml-email/joshua-uzzell-html-email-templates.png"

							 	),
			"project-type"	  => 	"project-type-interactive",
			"live-site"	  =>    false,
			"multiple-urls"	  =>  false,
			"live-url"	  =>    "",
			"git"		  =>    true,
			"git-url"     =>    "",
			"featured"    =>    false
		),
		"19" => array(
			"id"	      =>	19,
			"published"	  =>  "published",
			"slug"        =>    "cakewalk-app",
			"industry"	  =>	"E-commerce",
			"title"       => 	"Lightweight E-Commerce CMS",
			"client"      =>  	"The Cakewalk App - Personal Project",
			"roles"		  =>	array("Web Developer","Web Designer"),
			"tools"       =>  	array("Zurb Foundation", "MySQL", "PHP", "XHTML", "JQuery", "MAMP"),
			"process"     =>  	"Graphic Designer and Web Developer Joshua Uzzell, developed The Cakewalk App, as a lightweight alternative to using Wordpress or Joolma for managing online stores. His philosophy during development was, focus only on the features necessary for doing business online. <br/><br/>To that effect, The Cakewalk App exclusively features functions for displaying products, taking payments, tracking sells, and communicating with customers. By concentrating on these core features storeowners can readily perform tasks related to running their business without distraction.<br/><br/>The Cakewalk App is a rough framework divided into a customer-facing storefront, and a back office for managing products and tracking sells. Uzzell has gone as far as equipping the app with an automated HTML email system that notifies customers of purchases made by their account, order cancellations, and order completions. Furthermore all orders, records, and accounts are stored in a secure SQL Database. ",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Interactive"),
			"featImg"     =>  	$path."interactive/featured-images/feat-img_the-cake-walk-app.png",
			"space-images"=> 	true,
			"images"      =>  	array(
									$path."interactive/projects/joshua-uzzell_the-cakewalk-app.png"
							 	),
			"project-type"	  => 	"project-type-interactive",
			"live-site"	  =>    false,
			"multiple-urls"	  =>  false,
			"live-url"	  =>    "",
			"git"		  =>    true,
			"git-url"     =>    "",
			"featured"    =>    false
		),
		"20" => array(
			"id"	      =>	20,
			"published"	  =>  "published",
			"slug"        =>   "/barnett-campaign",
			"industry"	  =>	"Local Government",
			"title"       => 	"Website Mockup",
			"client"      =>  	"Personal Project",
			"tools"       =>  	array("Adobe Illustrator"),
			"roles"		  =>	array("Creative Direction","Graphic Designer"),
			"process"     =>  	"When Aaron J Barnett ran for Maryland Delegate in 2014, Graphic Designer Joshua Uzzell designed a Wordpress Theme for Barnett’s campaign as a personal project.<br/><br/>For Uzzell this was an opportunity to explore how to create visually effective calls-to-action. In his exploration, Uzzell developed visual patterns for presenting the candidate’s position as well as information regarding the campaign. Then he interrupted those patterns to draw visitors’ attention to points where action could be taken.",
			"tags"        => 	array("No tags available"),
			"categories"  =>  	array("Interactive"),
			"featImg"     =>  	$path."personal-projects/feat-img-aaron-j-barnett-joshua-uzzell-2.png",
			"space-images"=> 	false,
			"images"      =>  	array(
									$path."personal-projects/1-aaron-j-barnett-joshua-uzzell.png",
									$path."personal-projects/2-aaron-j-barnett-joshua-uzzell.png",
									$path."personal-projects/3-aaron-j-barnett-joshua-uzzell.png",

							 	),
			"project-type"	  => 	"project-type-interactive",
			"live-site"	  =>    false,
			"multiple-urls"	  =>  false,
			"live-url"	  =>    "",
			"git"		  =>    false,
			"git-url"     =>    "",
			"featured"    =>    true
		),
);
//Keep track of last Project[ID]
//Current number 26
?>
