<!DOCTYPE html>
<!-- HTML5 Mobile Boilerplate -->
<!--[if IEMobile 7]><html class="no-js iem7"><![endif]-->
<!--[if (gt IEMobile 7)|!(IEMobile)]><!--><html class="no-js" lang="en"><!--<![endif]-->

<!-- HTML5 Boilerplate -->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->

<head>

	<meta charset="utf-8">
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>Joshua Uzzell Graphic Design & Web Development</title>
	<meta name="description" content="Joshua Uzzell is an Interactive Web Developer based in the Baltimore.">
	<meta name="keywords" content="javascript, php, web development, graphic design, logos, illustration, web design">

	<meta name="author" content="http://www.joshuauzzell.com">

	<meta http-equiv="cleartype" content="on">
  
	<link rel="apple-touch-icon" sizes="57x57" href="<?php print_r($base_url.'app/assets/img/site-icon/apple-icon-57x57.png'); ?>">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php print_r($base_url.'app/assets/img/site-icon/apple-icon-60x60.png'); ?>">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php print_r($base_url.'app/assets/img/site-icon/apple-icon-72x72.png'); ?>">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php print_r($base_url.'app/assets/img/site-icon/apple-icon-76x76.png'); ?>">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php print_r($base_url.'app/assets/img/site-icon/apple-icon-114x114.png'); ?>">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php print_r($base_url.'app/assets/img/site-icon/apple-icon-120x120.png'); ?>">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php print_r($base_url.'app/assets/img/site-icon/apple-icon-144x144.png'); ?>">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php print_r($base_url.'app/assets/img/site-icon/apple-icon-152x152.png'); ?>">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php print_r($base_url.'app/assets/img/site-icon/apple-icon-180x180.png'); ?>">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php print_r($base_url.'app/assets/img/site-icon/android-icon-192x192.png'); ?>">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php print_r($base_url.'app/assets/img/site-icon/favicon-32x32.png'); ?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php print_r($base_url.'app/assets/img/site-icon/favicon-96x96.png'); ?>">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php print_r($base_url.'app/assets/img/site-icon/favicon-16x16.png'); ?>">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<!-- Responsive and mobile friendly stuff -->
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<?php print_r($base_url.'app/assets/src/css/bootstrap.css'); ?>" media="all">
	<link rel="stylesheet" href="<?php print_r($base_url.'app/assets/src/css/style.css'); ?>" media="all">
	<link rel="stylesheet" href="<?php print_r($base_url.'app/assets/src/css/jumbotron.css'); ?>" media="all">
	<link rel="stylesheet" href="<?php print_r($base_url.'app/assets/src/css/case-studies.css'); ?>" media="all">
	<link rel="stylesheet" href="<?php print_r($base_url.'app/assets/src/css/case-study-oliver-brewing.css'); ?>" media="all">
	<link rel="stylesheet" href="<?php print_r($base_url.'app/assets/src/css/case-study-northwind-report.css'); ?>" media="all">

	<!-- Google Analytics -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-43685594-2', 'auto');
	  ga('send', 'pageview');

	</script>

</head>
<body>
<?php
if($_SERVER["REMOTE_ADDR"] == "73.212.172.179"){
/*
<div id="lightbox1">
	<div id="lightbox1-content">
		<div id="lightbox1-project">
			<div id="lightbox1-main">
				<div id="lightbox1-project-response">

				</div>
				<div id="lightbox1-related-projects">

				</div>
			</div>
			<div id="lightbox1-sidebar">

			</div>

		</div>
	</div>
</div>
<div id="shade"></div>
*/
}
?>
<div class="container-fluid">
