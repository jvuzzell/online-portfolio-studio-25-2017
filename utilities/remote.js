
// Remote

var execSync = require( 'child_process' ).execSync;

module.exports = class client {

  constructor( user, host, env ) {
    this.user = user;
    this.host = host;
    this.env  = env;
  }

  exec( string ) {
    return execSync(
      `ssh -t -t ${ this.user }@${ this.host } '${ string }'`,
      { stdio: [ null, null, null ], env: this.env }
    );
  }

  copy( from, to, excluded ) {
    let exclude = '';

    if ( Array.isArray( excluded ) )
      exclude = excluded.reduce( ( line, item ) => `${ line } --exclude ${ item } `, '' );
    else if ( excluded )
      exclude = `--exclude ${ excluded }`;

    return execSync(
      `rsync -ave ssh --delete ${ exclude } ${ from } ${ this.user }@${ this.host }:${ to }`,
      { stdio: [ null, null, null ], env: this.env }
    );
  }
};
